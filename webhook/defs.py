"""Common variable definitions that can be used by all webhooks."""

EMAIL_BRIDGE_ACCOUNT = 'redhat-patchlab'
BOT_ACCOUNTS = ('cki-bot', 'cki-kwf-bot', EMAIL_BRIDGE_ACCOUNT)
KERNEL_BZ_BOT = 'cki-ci-bot+kernel-workflow-bugzilla@redhat.com'
ARK_PROJECT_ID = 13604247

KERNEL_PIPELINES = ['trigger_pipeline',
                    'merge_request_regular',
                    'merge_request_realtime',
                    'merge_request_private',
                    'merge_request_realtime_private',
                    'c9s_merge_request']
KERNEL_RT_PIPELINES = ['realtime_check',
                       'realtime_check_regular',
                       'realtime_check_private',
                       'realtime_check_c9s']

UMB_BRIDGE_MESSAGE_TYPE = 'cki.kwf.umb-bz-event'

LABELS_YAML_PATH = 'utils/labels.yaml'

NEEDS_REVIEW_LABEL_COLOR = '#FF0000'
NEEDS_TESTING_LABEL_COLOR = '#CAC542'
READY_LABEL_COLOR = '#428BCA'
NEEDS_REVIEW_SUFFIX = 'NeedsReview'
NEEDS_TESTING_SUFFIX = 'NeedsTesting'
TESTING_FAILED_SUFFIX = 'TestingFailed'
TESTING_WAIVED_SUFFIX = 'Waived'
READY_SUFFIX = 'OK'
TESTING_SUFFIXES = (NEEDS_TESTING_SUFFIX, TESTING_FAILED_SUFFIX)

BASE_DEPENDENCIES = ['Acks::OK',
                     'CKI::OK',
                     'CommitRefs::OK',
                     'Dependencies::OK',
                     'Signoff::OK']

READY_FOR_MERGE_DEPS = BASE_DEPENDENCIES + ['Bugzilla::OK']
READY_FOR_QA_DEPS = BASE_DEPENDENCIES + ['Bugzilla::NeedsTesting']

READY_FOR_MERGE_LABEL = \
    {'name': 'readyForMerge',
     'color': '#8BCA42',
     'description': ('All automated checks pass, this merge request should be suitable for '
                     'inclusion in main now.')
     }

READY_FOR_QA_LABEL = \
    {'name': 'readyForQA',
     'color': NEEDS_TESTING_LABEL_COLOR,
     'description': ('Basic checks pass, this merge request should be suitable for '
                     'testing by QA now.')
     }

EXT_TYPE_URL = 'https://gitlab.com/'
BZ_IN_DESCRIPTION_ONLY = 'DescOnly'
CODE_CHANGED_PREFIX = "CodeChanged::"
CKI_KERNEL_PREFIX = 'CKI'
CKI_KERNEL_RT_PREFIX = 'CKI_RT'
DCO_URL = "https://developercertificate.org"
DCO_PASS = "The DCO Signoff Check for all commits and the MR description has **PASSED**.\n"
DCO_FAIL = ("**ERROR: DCO 'Signed-off-by:' tags were not found on all commits and the MR "
            "description. Please review the results in the table below.**  \n"
            "This project requires developers add a per-commit acknowlegement of the [Developer "
            "Certificate of Origin](%s), also known as the DCO. This can be accomplished by "
            "adding an explicit 'Signed-off-by:' tag to each commit.\n\n"
            "**This Merge Request's commits will not be considered for inclusion into this "
            "project until these problems are resolved. After making the required changes please "
            "resubmit your merge request for review.**\n\n" % (DCO_URL))
SUBSYS_LABEL_PREFIX = 'Subsystem'
NOTIFICATION_HEADER = "Notifying users:"
NOTIFICATION_TEMPLATE = ("{header} {users}  \nThis is the Subsystems hook's user notification"
                         " system for file changes. Please see the"
                         " [kernel-watch project]({project}) for details.")
