"""Process bugzilla events from the UMB."""
from collections import defaultdict
import json
import sys
from threading import Lock
from threading import Thread
import time

from cki_lib import logger
from cki_lib.gitlab import get_instance
from gitlab.exceptions import GitlabListError
from pika.exceptions import AMQPError

from webhook import common
from webhook import defs

LOGGER = logger.get_logger('cki.webhook.umb_bridge')
SEND_DELAY = 1


def process_umb_event(body, bug_data, send_queue, send_message, **_):
    """Process UMB bugzilla events."""
    # Before we do anything check the sender thread is running.
    send_queue.check_status()

    if send_queue.send_function is None:
        LOGGER.info('Setting send_queue.send_message to: %s', send_message)
        send_queue.send_function = send_message

    bug = body['event'].get('bug_id')
    user = body['event'].get('user', {}).get('login')
    if bug is None or user is None:
        LOGGER.warning('No bug_id or user found in UMB event.')
        print(json.dumps(body, indent=2))
        return

    if bug not in bug_data:
        LOGGER.info('Ignoring event for unknown bug %d.', bug)
        return

    if user == defs.KERNEL_BZ_BOT:
        LOGGER.info('Ignoring event by bot user %s.', user)
        return

    LOGGER.info('Event for bug %d by %s relevant to these MRs: %s', bug, user, bug_data[bug])
    for mrpath in bug_data[bug]:
        send_queue.add(mrpath)


def process_gitlab_mr(message, bug_data, **_):
    """Process gitlab MR hook events."""
    action = message.payload['object_attributes'].get('action')
    changes = message.payload['changes']
    if action not in ('open', 'close', 'reopen') and 'description' not in changes:
        LOGGER.info('MR has not changed, ignoring event.')
        return

    description = message.payload['object_attributes'].get('description')
    mrpath = get_mr_reference(message.payload['object_attributes']['url'])

    mr_bugs = common.bugs_to_process(description, action, changes)

    bug_data.link_mr(mrpath, mr_bugs['link'])
    bug_data.unlink_mr(mrpath, mr_bugs['unlink'])


class BugData(defaultdict):
    """Hold mapping of bugs to MRs."""

    def __init__(self, projects):
        """Connect to GL and build the cache."""
        defaultdict.__init__(self, set)
        self.logger = logger.get_logger('cki.webhook.umb_bridge.BugData')

        self.gl_instance = get_instance('https://gitlab.com')
        self.gl_instance.auth()
        self.projects = projects
        self._build_cache()

    def _build_cache(self):
        """Populate a dictionary of MRs per bug."""
        mr_count = 0

        self.logger.info('Building cache from these projects: %s', self.projects)
        start_t = time.time()
        for project in self.projects:
            gl_project = self.gl_instance.projects.get(project)

            try:
                for gl_mr in gl_project.mergerequests.list(all=True, state='opened', view='simple'):
                    mr_count += 1
                    # Get all the bugs, including dependencies.
                    bug_list = set(common.extract_bzs(gl_mr.description))

                    mrpath = get_mr_reference(gl_mr.web_url)
                    self.link_mr(mrpath, bug_list)
            except GitlabListError as err:
                if err.response_code == 403:
                    self.logger.error("User '%s' does not have access to %s! Skipping.",
                                      self.gl_instance.user.username, project)
                else:
                    raise

        self.logger.info('MRs: %d, Bugs: %d. Processing time: %s',
                         mr_count, len(self), time.time() - start_t)

    def _update_mr(self, mrpath, bug_list, action):
        func = getattr(self, action)
        for bug in bug_list:
            # Filter out bugs such as 'INTERNAL' and convert to int.
            if isinstance(bug, int) or bug.isdigit():
                func(mrpath, int(bug))

    def _link_mr(self, mrpath, bug):
        self[bug].add(mrpath)
        self.logger.debug('BZ%s inserted %s.', bug, mrpath)

    def link_mr(self, mrpath, bug_list):
        """Insert MR."""
        self._update_mr(mrpath, bug_list, '_link_mr')

    def _unlink_mr(self, mrpath, bug):
        try:
            self[bug].remove(mrpath)
            self.logger.debug('BZ%s removed %s.', bug, mrpath)
        except KeyError:
            self.logger.warning("BZ%s can't remove %s, not in bug's set.", bug, mrpath)
        if not self[bug]:
            del self[bug]
            self.logger.debug('BZ%s removed.', bug)

    def unlink_mr(self, mrpath, bug_list):
        """Remove MR."""
        self._update_mr(mrpath, bug_list, '_unlink_mr')


def get_mr_reference(web_url):
    """Return an MR reference in 'full' format from the given the web_url."""
    web_url_split = web_url.split('/-/')
    namespace = '/'.join(web_url_split[0].split('/')[3:])
    mr_id = web_url_split[-1].split('/')[-1]
    return f'{namespace}!{mr_id}'


MSG_HANDLERS = {'merge_request': process_gitlab_mr,
                'amqp-bridge': process_umb_event
                }


class SendQueue(dict):
    """Manage queue of MRs to send to the bugzilla hook as a separate thread."""

    def __init__(self, sender_exchange, sender_route):
        """Set up send params and spawn thread."""
        dict.__init__(self)
        self.logger = logger.get_logger('cki.webhook.umb_bridge.SendQueue')

        # Must be set to MessageQueue.send_message() method before using the queue.
        self.send_function = None

        # Params passed by self._send_message to send_function, excluding headers.
        self.message_type = defs.UMB_BRIDGE_MESSAGE_TYPE
        self.send_params = {'data': None,
                            'queue_name': sender_route,
                            'exchange': sender_exchange
                            }

        self._lock = Lock()
        self._thread = Thread(target=self._sender_thread, daemon=True)

    def check_send_function(self):
        """Barf if send_function has not been set."""
        if self.send_function is None:
            self.logger.warning('send_function is not set!')
            return False
        return True

    def add(self, mrpath):
        """Add or update an entry in the queue."""
        if not self.check_send_function():
            return
        with self._lock:
            self.logger.info('Adding %s.', mrpath)
            # Remove it so we can depend on insertion order to test age.
            if mrpath in self:
                del self[mrpath]
            self[mrpath] = time.time()

    def _get(self):
        """Return the oldest item at least SEND_DELAY old."""
        # Caller must hold self.lock!
        if not self:
            self.logger.debug('Empty.')
            return None

        # Is the first (oldest) item at least SEND_DELAY old?
        last = next(iter(self))
        if self[last] > time.time() - SEND_DELAY:
            self.logger.debug('Nothing older than SEND_DELAY (%d).', SEND_DELAY)
            return None
        return last

    def _send_message(self, mrpath):
        """Send a message to the queue."""
        headers = {'message-type': self.message_type,
                   'mrpath': mrpath
                   }

        try:
            # pylint: disable=not-callable
            self.send_function(**self.send_params, headers=headers)
            self.logger.info('Sent %s.', mrpath)
        except AMQPError as err:
            self.logger.error('Error sending message: %s', err)
        del self[mrpath]

    def send_all(self):
        """Send all outstanding messages."""
        with self._lock:
            while (mrpath := self._get()) is not None:
                if not self.check_send_function():
                    return
                self._send_message(mrpath)
            self.logger.debug('Nothing to send.')

    def _sender_thread(self):
        """Process send queue."""
        while True:
            time.sleep(10)
            self.send_all()

    def check_status(self):
        """Confirm the sender thread is running, otherwise die."""
        # If the sender thread is not running try to send any messages in the
        # queue and then die.
        if not self._thread.is_alive():
            self.logger.error('Sender thread not running as expected.')
            with self._lock:
                for mrpath in self.copy():
                    self._send_message(mrpath)
                sys.exit(1)
        return True

    def start(self):
        """Start the sender thread."""
        self.logger.debug('Starting sender thread.')
        self._thread.start()
        self.check_status()


def route_keys_to_projects(keys):
    """Return a list of project paths given the list of routing keys."""
    projects = []
    for key in keys:
        if key.startswith('gitlab.com.') and key.endswith('.merge_request'):
            projects.append('/'.join(key.split('.')[2:-1]))
    return projects


def main(args):
    """Run main loop."""
    parser_prefix = 'UMB_BRIDGE'
    parser = common.get_arg_parser(parser_prefix)
    parser.add_argument('--rabbitmq-sender-exchange',
                        **common.get_argparse_environ_opts(f'{parser_prefix}_SENDER_EXCHANGE'),
                        help='RabbitMQ Exchange for sending messages.')
    parser.add_argument('--rabbitmq-sender-route',
                        **common.get_argparse_environ_opts(f'{parser_prefix}_SENDER_ROUTE'),
                        help='RabbitMQ Routing Key for sending messages.')
    args = parser.parse_args(args)

    # Convert routing keys to project namespaces.
    projects = route_keys_to_projects(args.rabbitmq_routing_key.split())

    # Initiate and start the send_queue handler.
    send_queue = SendQueue(args.rabbitmq_sender_exchange, args.rabbitmq_sender_route)
    send_queue.start()

    # Populate GL bug mapping and start consuming messages.
    bug_data = BugData(projects)
    common.consume_queue_messages(args, MSG_HANDLERS, bug_data=bug_data, send_queue=send_queue,
                                  get_gl_instance=False)


if __name__ == "__main__":
    main(sys.argv[1:])
