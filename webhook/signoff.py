"""Ensure MR commits have the necessary DCO signoff."""
import difflib
import enum
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib import session
from unidecode import unidecode

from . import common
from . import defs

LOGGER = logger.get_logger('cki.webhook.signoff')
SESSION = session.get_session('cki.webhook.signoff')


class State(enum.IntEnum):
    """Possible commit DCO check results."""

    OK = 0
    MISSING = 1
    NOMATCH = 2
    NOPUBLIC = 3


footnotes = {
    # State.OK: "A valid DCO Signoff was found for this commit.",

    State.MISSING: "No DCO Signoff was found for this commit.",
    State.NOMATCH: "Signed-off-by has bad format.",
    State.NOPUBLIC: ("MR Author account does not have a Public email address set! Click on your"
                     " avatar at the top right, click [Edit profile](https://gitlab.com/-/profile)"
                     ", and ensure that your Public email is set to your redhat.com address."),
}


def get_current_signoff_scope(labels):
    """Get current Signoff label scope."""
    for label in labels:
        if label.startswith("Signoff::"):
            return label.split(":")[2]
    return None


def find_dco(text, name, email):
    """Look for DCO string and return state."""
    state = State.MISSING
    expected_dco = f"Signed-off-by: {name} <{email}>"

    for line in text.splitlines():
        if unidecode(line.rstrip()) == unidecode(expected_dco):
            state = State.OK
            break
        if line.startswith("Signed-off-by:"):
            state = State.NOMATCH
            matched_sob = line.rstrip()

    if state == State.NOMATCH:
        LOGGER.info("DCO mismatch:")
        diff = difflib.unified_diff(expected_dco.split('\n'), matched_sob.split('\n'), lineterm="")
        for line in diff:
            if "Signed-off-by:" in line:
                LOGGER.info("%s", line)

    LOGGER.debug("Expected DCO: '%s' - %s", expected_dco, state.name)
    return (state, expected_dco.removeprefix('Signed-off-by: '))


def process_commits(project, mr_commits):
    """For a given set of commits return a dict of {commit.id: State}."""
    commits = {}
    for commit in mr_commits:
        commit = project.commits.get(commit.id)
        # Skip merge commits
        if len(commit.parent_ids) > 1:
            LOGGER.debug("Merge commit? Skipping.")
            continue
        commits[commit.id] = find_dco(commit.message, commit.author_name, commit.author_email)
    return commits


def generate_table(commits):
    """Generate a markdown table of results."""
    # header
    results_table = "**DCO Signoff "
    if all(state[0] == State.OK for state in commits.values()):
        results_table += "Report**\n\n"
        results_table += defs.DCO_PASS
        if misc.is_production():
            return results_table
    else:
        results_table += "Error(s)!**\n\n"
        results_table += defs.DCO_FAIL
    # table of results
    results_table += "| **Commit** | **Signoff Status** |\n|----|----|\n"
    for commit, result in commits.items():
        state = result[0]
        if state is State.OK:
            if not misc.is_production():
                results_table += "| %s | %s |\n" % (commit, state.name)
        else:
            results_table += "| %s | **%s** [^%d] |\n" % (commit, state.name, state.value)
    # footer (footnotes)
    for state, text in footnotes.items():
        results_table += "[^%d]: %s\n" % (state.value, text)
    return results_table


def update_mr(gl_instance, gl_project, merge_request, results_table, log_ok_scope):
    """Update the MR with a note of the results and possibly set the label scope."""
    current_scope = get_current_signoff_scope(merge_request.labels)
    if defs.DCO_PASS in results_table:
        new_scope = "OK"
    else:
        new_scope = "NeedsReview"
    LOGGER.debug("current scope: '%s', new scope: '%s'", current_scope, new_scope)

    common.add_label_to_merge_request(gl_instance, gl_project, merge_request.iid,
                                      [f'Signoff::{new_scope}'])

    if new_scope == "OK" and not log_ok_scope:
        return

    if misc.is_production():
        merge_request.notes.create({'body': results_table})


def _do_process_mr(gl_instance, message, mr_id, log_ok_scope):
    gl_project = gl_instance.projects.get(message.payload["project"]["id"])
    if not (gl_mergerequest := common.get_mr(gl_project, mr_id)):
        return

    dco_results = process_commits(gl_project, gl_mergerequest.commits())

    # Check the MR description too.
    mr_name = gl_mergerequest.author['name']
    mr_email = gl_instance.users.get(gl_mergerequest.author['id']).public_email
    if not mr_email:
        dco_results['MR Description'] = (State.NOPUBLIC, f'{mr_name} <UNKNOWN>')
    else:
        dco_results['MR Description'] = find_dco(gl_mergerequest.description, mr_name, mr_email)

    results_table = generate_table(dco_results)
    update_mr(gl_instance, gl_project, gl_mergerequest, results_table, log_ok_scope)

    LOGGER.debug("Results table:\n%s", results_table)


def process_mr(gl_instance, message, **_):
    """Process a merge request message."""
    desc_changed = 'description' in message.payload.get('changes', {})
    label_changed = common.has_label_changed(message.payload, 'Signoff::', common.LabelPart.PREFIX)
    if not common.mr_action_affects_commits(message) and not desc_changed and not label_changed:
        return

    _do_process_mr(gl_instance, message, message.payload["object_attributes"]["iid"], label_changed)


def process_note(gl_instance, message, **_):
    """Process a merge request only if request-signoff-evluation was specified."""
    if not common.force_webhook_evaluation(message.payload['object_attributes']['note'], 'signoff'):
        return

    _do_process_mr(gl_instance, message, message.payload["merge_request"]["iid"], True)


WEBHOOKS = {
    "merge_request": process_mr,
    "note": process_note,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('SIGNOFF')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS)


if __name__ == "__main__":
    main(sys.argv[1:])
