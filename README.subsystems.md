# Subsystems Webhook

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.subsystems \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
        --owners-yaml owners.yml \
	--local-repo-path /data/kernel-watch \
	--kernel-watch-url https://gitlab.com/ptalbert/kernel-watch

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

The `--map-file` argument can also be passed to the script via the `MAP_FILE`
environment variable, `--local-repo-path` with `LOCAL_REPO_PATH`, and
'--kernel-watch-url` with KERNEL_WATCH_URL.
