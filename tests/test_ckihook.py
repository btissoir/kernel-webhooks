"""Webhook interaction tests."""
import copy
from unittest import TestCase
from unittest import mock

from webhook import ckihook
from webhook import defs


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCkihook(TestCase):
    """ Test Webhook class."""

    PAYLOAD_MR = {'object_kind': 'merge_request',
                  'project': {'id': 1, 'path_with_namespace': 'group/project'},
                  'object_attributes': {'iid': 123},
                  'user': {'username': 'gitlab-user'}
                  }

    PAYLOAD_PIPELINE = {'object_kind': 'pipeline',
                        'project': {'id': 1, 'path_with_namespace': 'group/project'},
                        'merge_request': {'iid': 123},
                        'object_attributes': {'id': 23456},
                        'user': {'username': 'gitlab-user'}
                        }

    MOCK_PIPELINE_1 = {'status': 'SUCCESS',
                       'sourceJob': {'name': 'trigger_pipeline'},
                       'jobs': {'nodes': []}
                       }

    MOCK_PIPELINE_2 = {'status': 'FAILED',
                       'sourceJob': {'name': 'realtime_check_regular'},
                       'jobs': {'nodes': [{'status': 'FAILED', 'stage': {'name': 'merge'}}]}
                       }

    MOCK_LABEL_1 = {'title': 'Acks::OK',
                    'color': '#428BCA',
                    'description': 'Great'
                    }

    MOCK_LABEL_2 = {'title': 'CKI::OK',
                    'color': '#428BCA',
                    'description': "This MR's latest CKI pipeline was successful."
                    }

    MOCK_LABEL_3 = {'title': 'CKI_RT::Failed::build',
                    'color': '#6242ca',
                    'description': "This MR's latest CKI_RT pipeline has failed or been canceled."
                    }

    @mock.patch('webhook.ckihook.get_instance')
    @mock.patch('webhook.common.add_label_to_merge_request')
    def test_update_cki_labels(self, mock_add_label, mock_get_instance):
        mock_pipe1 = mock.Mock(status=ckihook.Status.Success, new_label='CKI::Success',
                               label_prefix='CKI')
        mock_pipe2 = mock.Mock(status=ckihook.Status.Running, new_label='CKI_RT::Running',
                               label_prefix='CKI_RT')
        mock_pipelines = [mock_pipe1, mock_pipe2]
        labels = ['CKI::Success', 'CKI_RT::Running']
        ckihook.update_cki_labels('namespace', 123, mock_pipelines)
        mock_add_label.assert_called_with(mock_get_instance.return_value,
                                          mock_get_instance().projects.get.return_value,
                                          123, labels, remove_scoped=True)

    def test_MrPipeline(self):
        # A succesful CKI pipeline with no label change
        label_list = [self.MOCK_LABEL_1, self.MOCK_LABEL_2, self.MOCK_LABEL_3]
        pipe = ckihook.MrPipeline(self.MOCK_PIPELINE_1, label_list)
        self.assertFalse(pipe.label_changed)
        self.assertEqual(pipe.label_prefix, defs.CKI_KERNEL_PREFIX)
        self.assertEqual(pipe.existing_label, 'CKI::OK')
        self.assertEqual(pipe.new_label, 'CKI::OK')
        self.assertEqual(pipe.name, 'trigger_pipeline')
        self.assertEqual(pipe.status, ckihook.Status.Success)
        self.assertEqual(pipe.failed_stage, None)

        # A failed CKI_RT pipeline with a label change.
        pipe = ckihook.MrPipeline(self.MOCK_PIPELINE_2, label_list)
        self.assertTrue(pipe.label_changed)
        self.assertEqual(pipe.label_prefix, defs.CKI_KERNEL_RT_PREFIX)
        self.assertEqual(pipe.existing_label, 'CKI_RT::Failed::build')
        self.assertEqual(pipe.new_label, 'CKI_RT::Failed::merge')
        self.assertEqual(pipe.name, 'realtime_check_regular')
        self.assertEqual(pipe.status, ckihook.Status.Failed)
        self.assertEqual(pipe.failed_stage, 'merge')

        # No pipeline???
        pipe = ckihook.MrPipeline([], label_list)
        self.assertFalse(pipe.label_changed)
        self.assertEqual(pipe.label_prefix, None)
        self.assertEqual(pipe.existing_label, None)
        self.assertEqual(pipe.new_label, None)
        self.assertEqual(pipe.name, None)
        self.assertEqual(pipe.status, ckihook.Status.Unknown)
        self.assertEqual(pipe.failed_stage, None)

    @mock.patch('webhook.common.gl_graphql_client')
    def test_do_ckihook_query(self, mock_client):
        username = 'gitlab-user'
        query_params = {'namespace': 'redhat/src/kernel/rhel-8', 'mr_id': 123}

        # Not the expected data.
        mock_client.return_value.__enter__.return_value.execute.return_value = {'project': None}
        with self.assertLogs('cki.webhook.ckihook', level='ERROR') as logs:
            result = ckihook.do_ckihook_query(username, query_params)
            self.assertEqual(result, None)
            self.assertIn('Gitlab did not return the expected data', logs.output[-1])

        # Bot message.
        mock_data = {'currentUser': {'username': username}, 'project': {}}
        mock_client.return_value.__enter__.return_value.execute.return_value = mock_data
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            result = ckihook.do_ckihook_query(username, query_params)
            self.assertEqual(result, None)
            self.assertIn('Ignoring bot message.', logs.output[-1])

        # Good data.
        mock_data['currentUser'] = {'username': 'steve'}
        mock_data['project'] = {'mergeRequest': {}}
        result = ckihook.do_ckihook_query(username, query_params)
        self.assertEqual(result, {})

    @mock.patch('webhook.ckihook.update_cki_labels')
    @mock.patch('webhook.ckihook.do_ckihook_query')
    def test_compute_labels(self, mock_do_query, mock_update_cki_labels):
        username = 'gitlab-user'
        namespace = 'redhat/src/kernel/rhel-8'
        mr_id = 1234
        label_list = [self.MOCK_LABEL_1, self.MOCK_LABEL_2, self.MOCK_LABEL_3]

        # No query data.
        mock_do_query.return_value = None
        ckihook.compute_labels(username, namespace, mr_id)
        mock_update_cki_labels.assert_not_called()

        mock_data = {'labels': {'nodes': label_list},
                     'headPipeline': None
                     }

        # No head pipeline.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            mock_do_query.return_value = mock_data

            ckihook.compute_labels(username, namespace, mr_id)
            self.assertIn('No Head Pipeline', logs.output[-3])
            self.assertIn('to CKI::Missing', logs.output[-2])
            self.assertIn('to CKI_RT::Missing', logs.output[-1])
            mock_update_cki_labels.assert_called_once()

        mock_update_cki_labels.reset_mock()
        # CKI pipeline with no label change, CKI_RT with label change.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            downstream = {'downstream': {'nodes': [self.MOCK_PIPELINE_1, self.MOCK_PIPELINE_2]}}
            mock_data['headPipeline'] = downstream
            mock_do_query.return_value = mock_data

            ckihook.compute_labels(username, namespace, mr_id)
            self.assertIn('trigger_pipeline label has not changed: CKI::OK', logs.output[-2])
            self.assertIn('realtime_check_regular label has changed', logs.output[-1])
            mock_update_cki_labels.assert_called_once()

    @mock.patch('webhook.common.has_label_changed')
    @mock.patch('webhook.ckihook.compute_labels')
    def test_process_message(self, mock_compute, mock_has_label):
        username = 'gitlab-user'

        # Some weird unknown msg type.
        msg = mock.Mock(payload={'object_kind': 'note'})
        with self.assertLogs('cki.webhook.ckihook', level='WARNING') as logs:
            ckihook.process_message(msg)
            self.assertIn('Ignoring event with unexpected object_kind', logs.output[-1])
            mock_compute.assert_not_called()

        # Pipeline msg, no MR.
        pipe_msg = copy.deepcopy(self.PAYLOAD_PIPELINE)
        pipe_msg['merge_request'] = None
        msg = mock.Mock(payload=pipe_msg)
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            ckihook.process_message(msg)
            self.assertIn('Pipeline 23456 is not associated with an MR, ignoring.',
                          logs.output[-1])
            mock_compute.assert_not_called()

        # Pipeline msg, good to go.
        mock_compute.reset_mock()
        pipe_msg = copy.deepcopy(self.PAYLOAD_PIPELINE)
        msg = mock.Mock(payload=pipe_msg)
        ckihook.process_message(msg)
        mock_compute.assert_called_with(username, 'group/project', 123)

        # MR message, MR has CKI labels and none have changed so do not run compute_labels().
        mock_compute.reset_mock()
        mr_msg = copy.deepcopy(self.PAYLOAD_MR)
        mr_msg['labels'] = [self.MOCK_LABEL_1, self.MOCK_LABEL_2, self.MOCK_LABEL_3]
        msg = mock.Mock(payload=mr_msg)
        mock_has_label.side_effect = [False, False]
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            ckihook.process_message(msg)
            self.assertIn('MR 123 already has CKI labels, ignoring event.', logs.output[-1])
            mock_compute.assert_not_called()

        # MR message, MR does not already have CKI_RT label so run compute_labels().
        mock_compute.reset_mock()
        mock_has_label.reset_mock(side_effect=True)
        mr_msg = copy.deepcopy(self.PAYLOAD_MR)
        mr_msg['labels'] = [self.MOCK_LABEL_1, self.MOCK_LABEL_2]
        msg = mock.Mock(payload=mr_msg)
        mock_has_label.side_effect = [False, False]

        ckihook.process_message(msg)
        mock_compute.assert_called_with(username, 'group/project', 123)

        # MR message, MR has CKI labels but one has changed so run compute_labels().
        mock_compute.reset_mock()
        mock_has_label.reset_mock(side_effect=True)
        mr_msg = copy.deepcopy(self.PAYLOAD_MR)
        mr_msg['labels'] = [self.MOCK_LABEL_1, self.MOCK_LABEL_2, self.MOCK_LABEL_3]
        msg = mock.Mock(payload=mr_msg)
        mock_has_label.side_effect = [True, False]

        ckihook.process_message(msg)
        mock_compute.assert_called_with(username, 'group/project', 123)
