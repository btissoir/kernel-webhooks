"""Webhook interaction tests."""
import json
import os
import pathlib
import tempfile
from unittest import TestCase
from unittest import mock

from gitlab.exceptions import GitlabCreateError
from gitlab.exceptions import GitlabGetError

from webhook import common
from webhook import defs


class MyLister:
    def __init__(self, labels):
        self.labels = labels
        self.call_count = 0
        self.called_with_all = None

    def list(self, search=None, all=False):
        self.call_count += 1
        if all:
            self.called_with_all = True
            return self.labels
        self.called_with_all = False
        if not search:
            raise AttributeError
        return [label for label in self.labels if label.name == search]


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCommon(TestCase):
    PROJECT_LABELS = [{'id': 1,
                       'name': 'readyForMerge',
                       'color': defs.READY_FOR_MERGE_LABEL['color'],
                       'description': ('All automated checks pass, this merge request'
                                       ' should be suitable for inclusion in main now.'),
                       'text_color': '#FFFFFF',
                       'priority': 1
                       },
                      {'id': 2,
                       'name': 'readyForQA',
                       'color': defs.READY_FOR_QA_LABEL['color'],
                       'description': ('This merge request has been explicitly Nacked'
                                       ' by Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 3,
                       'name': 'Acks::NACKed',
                       'color': defs.NEEDS_REVIEW_LABEL_COLOR,
                       'description': ('This merge request has been explicitly Nacked'
                                       ' by Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 4,
                       'name': 'Acks::NeedsReview',
                       'color': defs.NEEDS_REVIEW_LABEL_COLOR,
                       'description': ('This merge request needs more reviews and acks'
                                       ' from Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 3
                       },
                      {'id': 5,
                       'name': 'Acks::OK',
                       'color': defs.READY_LABEL_COLOR,
                       'description': ('This merge request has been reviewed by Red Hat'
                                       ' engineering and approved for inclusion.'),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 6,
                       'name': 'Bugzilla::OK',
                       'color': defs.READY_LABEL_COLOR,
                       'description': ("This merge request's bugzillas are approved for"
                                       " integration"),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 7,
                       'name': 'Bugzilla::NeedsTesting',
                       'color': defs.NEEDS_TESTING_LABEL_COLOR,
                       'description': "This merge request's bugzillas are ready for QA testing.",
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 8,
                       'name': 'lnst::NeedsTesting',
                       'color': defs.NEEDS_TESTING_LABEL_COLOR,
                       'description': ('The subsystem-required lnst testing has not yet'
                                       ' been completed.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 9,
                       'name': 'lnst::OK',
                       'color': defs.READY_LABEL_COLOR,
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 10,
                       'name': 'Dependencies::abcdef012345',
                       'color': '#123456',
                       'description': 'This MR has dependencies.',
                       'text_color': '#FFFFFF',
                       'priority': 15
                       },
                      {'id': 11,
                       'name': 'Acks::net::NeedsReview',
                       'color': defs.NEEDS_TESTING_LABEL_COLOR,
                       'descroption': 'Subsystem needs Acks'
                       },
                      {'id': 12,
                       'name': 'CKI::Failed::test',
                       'color': defs.NEEDS_TESTING_LABEL_COLOR,
                       'description': 'Failed CKI testing'
                       }]

    MAPPINGS = {'subsystems':
                [{'subsystem': 'MEMORY MANAGEMENT',
                  'labels': {'name': 'mm'},
                  'paths': {'includes': ['include/linux/mm.h', 'include/linux/vmalloc.h', 'mm/']}
                  },
                 {'subsystem': 'NETWORKING',
                  'labels': {'name': 'net',
                             'readyForMergeDeps': ['lnst']},
                  'paths': {'includes': ['include/linux/net.h', 'include/net/', 'net/']},
                  },
                 {'subsystem': 'XDP',
                  'labels': {'name': 'xdp'},
                  'paths': {'includes': ['kernel/bpf/devmap.c', 'include/net/page_pool.h'],
                            'includeRegexes': ['xdp']}
                  }]
                }

    FILE_CONTENTS = (
        '---\n'
        'subsystems:\n'
        ' - subsystem: MEMORY MANAGEMENT\n'
        '   labels:\n'
        '     name: mm\n'
        '   paths:\n'
        '       includes:\n'
        '          - include/linux/mm.h\n'
        '          - include/linux/vmalloc.h\n'
        '          - mm/\n'
        ' - subsystem: NETWORKING\n'
        '   labels:\n'
        '     name: net\n'
        '     readyForMergeDeps:\n'
        '       - lnst\n'
        '   paths:\n'
        '       includes:\n'
        '          - include/linux/net.h\n'
        '          - include/net/\n'
        '          - net/\n'
        ' - subsystem: XDP\n'
        '   labels:\n'
        '     name: xdp\n'
        '   paths:\n'
        '       includes:\n'
        '          - kernel/bpf/devmap.c\n'
        '          - include/net/page_pool.h\n'
        '       includeRegexes:\n'
        '          - xdp\n'
        )

    YAML_LABELS = [{'name': 'Acks::OK', 'color': '#428BCA', 'description': 'all good'},
                   {'name': 'Bugzilla::OnQA', 'color': '#CAC542', 'description': 'maybe'},
                   {'name': '^Subsystem:(\\w+)$',
                    'color': '#778899',
                    'description': 'hi %s',
                    'regex': True}
                   ]

    def test_mr_is_closed(self):
        mrequest = mock.Mock()
        mrequest.state = "closed"
        status = common.mr_is_closed(mrequest)
        self.assertTrue(status)
        mrequest.state = "opened"
        status = common.mr_is_closed(mrequest)
        self.assertFalse(status)

    def test_build_note_string(self):
        notes = []
        notes += ["1"]
        notes += ["2"]
        notes += ["3"]
        notestring = common.build_note_string(notes)
        self.assertEqual(notestring, "See 1, 2, 3|\n")

    def test_build_commits_for_row(self):
        commits = ["abcdef012345"]
        table = [["012345abcdef", commits, 1, "", ""]]
        for row in table:
            commits = common.build_commits_for_row(row)
            self.assertEqual(commits, ["abcdef01"])

    def test_mr_action_affects_commits(self):
        """Check handling of a merge request with unwanted actions."""
        message = mock.Mock()
        # 'open' action should return True.
        payload = {'object_attributes': {'action': 'open'}}
        message.payload = payload
        self.assertTrue(common.mr_action_affects_commits(message))

        # 'update' action with oldrev set should return True.
        payload = {'object_attributes': {'action': 'update',
                                         'oldrev': 'hi'}
                   }
        message.payload = payload
        self.assertTrue(common.mr_action_affects_commits(message))

        # 'update' action without oldrev should return False and log it.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload = {'object_attributes': {'action': 'update'}}
            message.payload = payload
            self.assertFalse(common.mr_action_affects_commits(message))
            self.assertIn("Ignoring MR \'update\' action without an oldrev.", logs.output[-1])

        # Action other than 'new' or 'update' should return False and log it.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload = {'object_attributes': {'action': 'partyhard'}}
            message.payload = payload
            self.assertFalse(common.mr_action_affects_commits(message))
            self.assertIn("Ignoring MR action 'partyhard'", logs.output[-1])

        # Target branch changed should return True
        payload = {'object_attributes': {'action': 'update'},
                   'changes': {'merge_status': {'previous': 'can_be_merged',
                                                'current': 'unchecked'}}}
        message.payload = payload
        self.assertTrue(common.mr_action_affects_commits(message))

    def test_required_label_removed(self):
        payload = {'object_attributes': {'action': 'update'}}
        testing = defs.NEEDS_TESTING_SUFFIX
        ready = defs.READY_SUFFIX
        failed = defs.TESTING_FAILED_SUFFIX
        waived = defs.TESTING_WAIVED_SUFFIX

        changed_labels = ['lnst::NeedsTesting', 'whatever::OK']
        result = common.required_label_removed(payload, testing, changed_labels)
        self.assertEqual(result, True)

        changed_labels = ['Drivers:bonding']
        result = common.required_label_removed(payload, testing, changed_labels)
        self.assertEqual(result, False)

        changed_labels = ['rdmalab::OK']
        result = common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, True)

        changed_labels = ['Bugzilla::OK']
        result = common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, False)

        changed_labels = ['lnst::TestingFailed']
        result = common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, False)

        changed_labels = ['lnst::TestingFailed']
        result = common.required_label_removed(payload, failed, changed_labels)
        self.assertEqual(result, True)

        changed_labels = ['lnst::Waived']
        result = common.required_label_removed(payload, waived, changed_labels)
        self.assertEqual(result, True)

    def test_has_label_changed(self):
        msg = {'changes': {}}
        full = common.LabelPart.FULL
        prefix = common.LabelPart.PREFIX

        # No 'labels' key
        self.assertFalse(common.has_label_changed(msg, 'ack_nack', prefix))

        msg['changes']['labels'] = {}

        # No label changes
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'}]
        self.assertFalse(common.has_label_changed(msg, 'ack_nack::', prefix))

        # Label without prefix changes.
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'},
                                                {'title': 'readyForMerge'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'}]
        self.assertTrue(common.has_label_changed(msg, 'readyForMerge', full))

        # Label without prefix does not change.
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'},
                                                {'title': 'readyForMerge'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'},
                                               {'title': 'readyForMerge'}]
        self.assertFalse(common.has_label_changed(msg, 'readyForMerge', full))

        # Change a label not related to ack/nack
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'},
                                                {'title': 'somethingelse::OK'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'}]
        self.assertFalse(common.has_label_changed(msg, 'ack_nack::', prefix))

        # Remove ack_nack label
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'},
                                                {'title': 'somethingelse::OK'}]
        msg['changes']['labels']['current'] = [{'title': 'somethingelse::OK'}]
        self.assertTrue(common.has_label_changed(msg, 'ack_nack', prefix))

        # Add ack_nack label
        msg['changes']['labels']['previous'] = [{'title': 'somethingelse::OK'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'},
                                               {'title': 'somethingelse::OK'}]
        self.assertTrue(common.has_label_changed(msg, 'ack_nack', prefix))

    def test_find_extra_required_labels(self):
        labels = ['lnst::NeedsTesting']
        result = common._find_extra_required_labels(labels)
        self.assertEqual(result, ['lnst::OK'])

    def _process_message(self, mock_gl, msg, auth_user):
        webhooks = {'note': mock.Mock()}

        fake_gitlab = mock.Mock(spec=[])

        def mock_auth():
            fake_gitlab.user = mock.Mock(username=auth_user, _setup_from="mock_auth")

        fake_gitlab.auth = mock_auth
        if auth_user:
            fake_gitlab.user = mock.Mock(username=auth_user, _setup_from="init")

        mock_gl.return_value.__enter__.return_value = fake_gitlab

        with tempfile.NamedTemporaryFile() as tmp:
            pathlib.Path(tmp.name).write_text(json.dumps(msg, indent=None))
            parser = common.get_arg_parser('test')
            args = parser.parse_args(['--json-message-file', tmp.name])
            common.consume_queue_messages(args, webhooks)

        if auth_user:
            self.assertEqual(fake_gitlab.user._setup_from, "init")
        else:
            self.assertEqual(fake_gitlab.user._setup_from, "mock_auth")

        return webhooks['note']

    @mock.patch('webhook.common.Message.gl_instance')
    def test_cmdline_json_processing(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    def test_cmdline_no_auth(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, None)
        webhook.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    def test_cmdline_bot_username(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'cki-bot'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_not_called()

    @mock.patch('webhook.common.Message.gl_instance')
    def test_cmdline_regular_username(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_called_once()

    def test_make_payload(self):
        # Note payload
        payload = common.make_payload('https://web.url/g/p/-/merge_requests/123', 'note')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], 'g/p')
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'note')
        self.assertEqual(payload['object_attributes']['noteable_type'], 'MergeRequest')
        self.assertEqual(payload['merge_request']['iid'], 123)

        # MR payload
        payload = common.make_payload('https://web.url/g/p/-/merge_requests/123', 'merge_request')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], 'g/p')
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'merge_request')
        self.assertEqual(payload['object_attributes']['iid'], 123)
        self.assertFalse(payload['object_attributes']['work_in_progress'])

        # Pipeline payload
        payload = common.make_payload('https://web.url/g/p/-/merge_requests/123', 'pipeline')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], 'g/p')
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'pipeline')
        self.assertEqual(payload['merge_request']['iid'], 123)

        # Some other payload?
        payload = common.make_payload('https://web.url/g/p/-/merge_requests/123', 'lion_attack')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], 'g/p')
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'lion_attack')
        self.assertEqual(payload['_mr_id'], 123)

    def test_process_mr_url(self):
        url = 'https://web.url/g/p/-/merge_requests/123'

        # Note
        note_text = 'request-evaluation'
        payload = common.process_mr_url(url, 'note', note_text)
        self.assertEqual(payload['object_kind'], 'note')
        self.assertEqual(payload["object_attributes"]["note"], note_text)

        # MR
        payload = common.process_mr_url(url, 'merge_request', None)
        self.assertEqual(payload['object_kind'], 'merge_request')

        # Pipeline
        payload = common.process_mr_url(url, 'pipeline', None)
        self.assertEqual(payload['object_kind'], 'pipeline')

    @mock.patch('webhook.common.consume_queue_messages')
    @mock.patch('webhook.common.process_mr_url')
    @mock.patch('webhook.common.process_message')
    def test_generic_loop(self, mock_process_message, mock_mr_url, mock_consume):
        WEBHOOKS = {}
        mr_url = 'https://web.url/g/p/-/merge_requests/123'
        headers = {'message-type': 'gitlab'}

        # --merge-request, no --note, no --action
        args = mock.Mock(merge_request=mr_url, action=None, note=None)
        common.generic_loop(args, WEBHOOKS)
        mock_consume.assert_not_called()
        mock_mr_url.assert_called_with(mr_url, 'merge_request', None)
        mock_process_message.assert_called_with(WEBHOOKS, 'cmdline', mock_mr_url.return_value,
                                                headers)

        # --merge-request with --note, no --action
        note_text = 'request-evaluation'
        args = mock.Mock(merge_request=mr_url, action=None, note=note_text)
        common.generic_loop(args, WEBHOOKS)
        mock_consume.assert_not_called()
        mock_mr_url.assert_called_with(mr_url, 'note', note_text)
        mock_process_message.assert_called_with(WEBHOOKS, 'cmdline', mock_mr_url.return_value,
                                                headers)

        # --merge-request and --action set, no --note.
        args = mock.Mock(merge_request=mr_url, action='pipeline', note=None)
        common.generic_loop(args, WEBHOOKS)
        mock_consume.assert_not_called()
        mock_mr_url.assert_called_with(mr_url, 'pipeline', None)
        mock_process_message.assert_called_with(WEBHOOKS, 'cmdline', mock_mr_url.return_value,
                                                headers)

        # no --merge-request
        mock_mr_url.reset_mock()
        mock_process_message.reset_mock()
        args = mock.Mock(merge_request=None)
        common.generic_loop(args, WEBHOOKS)
        mock_mr_url.assert_not_called()
        mock_process_message.assert_not_called()
        mock_consume.assert_called_with(args, WEBHOOKS)

    def test_process_umb_bridge_message(self):
        """Test processing of umb_bridge messages."""
        # No handler.
        webhooks = {}
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            result = common._process_umb_bridge_message('headers', webhooks)
            self.assertFalse(result)
            self.assertIn(f'No {defs.UMB_BRIDGE_MESSAGE_TYPE} handler, ignoring',
                          logs.output[-1])

        # A handler.
        mock_handler = mock.Mock()
        webhooks = {defs.UMB_BRIDGE_MESSAGE_TYPE: mock_handler}
        result = common._process_umb_bridge_message('headers', webhooks)
        self.assertTrue(result)
        mock_handler.assert_called_with('headers')

    def test_process_amqp_bridge_message(self):
        """Test processing of amqp-bridge messages."""
        # No amqp-bridge handler.
        webhooks = {}
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            result = common._process_amqp_bridge_message('body', webhooks)
            self.assertFalse(result)
            self.assertIn('No amqp-bridge handler, ignoring', logs.output[-1])

        # A handler.
        mock_handler = mock.Mock()
        webhooks = {'amqp-bridge': mock_handler}
        result = common._process_amqp_bridge_message('body', webhooks)
        self.assertTrue(result)
        mock_handler.assert_called_with('body')

    @mock.patch('webhook.common._process_umb_bridge_message')
    @mock.patch('webhook.common._process_amqp_bridge_message')
    @mock.patch('webhook.common._process_gitlab_message')
    def test_process_message(self, mock_gitlab, mock_amqp, mock_umb):
        """Test message types."""
        # Handle a gitlab message.
        headers = {'message-type': 'gitlab'}
        common.process_message('webhooks', 'routing.key', 2, headers)
        mock_gitlab.assert_called_with(2, 'webhooks')

        # Handle an amqp message.
        headers = {'message-type': 'amqp-bridge'}
        common.process_message('webhooks', 'routing.key', 2, headers)
        mock_amqp.assert_called_with(2, 'webhooks')

        # Handle a umb_bridge message.
        headers = {'message-type': defs.UMB_BRIDGE_MESSAGE_TYPE}
        common.process_message('webhooks', 'routing.key', 2, headers)
        mock_umb.assert_called_with(headers, 'webhooks')

        # Complain about unknown message type.
        headers = {'message-type': 'pigeon'}
        with self.assertLogs('cki.webhook.common', level='WARNING') as logs:
            result = common.process_message({}, 'routing.key', '', headers)
            self.assertTrue(result)
            self.assertIn('Ignoring unknown message type pigeon', logs.output[-1])

    @mock.patch('json.dumps')
    def test_process_gitlab_message(self, dumps):
        """Check for expected return value."""
        webhooks = {'merge_request': mock.Mock(), 'note': mock.Mock()}
        payload = {}

        # Wrong object kind should be ignored, return False.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload['object_kind'] = 'delete_project'
            self.assertFalse(common._process_gitlab_message(payload, webhooks))
            self.assertIn('Ignoring message from queue', logs.output[-1])

        # Closed state should be ignored, return False.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload['object_kind'] = 'merge_request'
            payload['state'] = 'closed'
            self.assertFalse(common._process_gitlab_message(payload, webhooks))
            self.assertIn("Ignoring event with 'closed' state.", logs.output[-1])

        # If username matches return False (event was generated by our own activity).
        with mock.patch('webhook.common.Message.gl_instance') as mocked_gl:
            mocked_gl().__enter__().user = mock.Mock(username='bot')
            payload['object_kind'] = 'merge_request'
            payload['state'] = 'opened'
            payload['user'] = {'username': 'bot'}
            self.assertFalse(common._process_gitlab_message(payload, webhooks))
            payload['user'] = {'username': 'notbot'}
            self.assertTrue(common._process_gitlab_message(payload, webhooks))

        # Ignore notes on issues.
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            payload['object_kind'] = 'note'
            payload['object_attributes'] = {'noteable_type': 'Issue'}
            self.assertFalse(common._process_gitlab_message(payload, webhooks))
            self.assertIn('Only processing notes related to merge requests', logs.output[-1])

        # Notes on merge requests should be processed.
        with self.assertLogs('cki.webhook.common', level='INFO'), \
             mock.patch('webhook.common.Message.gl_instance') as mocked_gl:
            mocked_gl().__enter__().user = mock.Mock(username='bob')
            payload['object_kind'] = 'note'
            payload['object_attributes'] = {'noteable_type': 'MergeRequest'}
            self.assertTrue(common._process_gitlab_message(payload, webhooks))
            self.assertEqual(len(webhooks['merge_request'].call_args.args), 2)

        # Recognize get_gl_instance=False.
        with mock.patch('webhook.common.Message.gl_instance') as mocked_gl:
            mocked_gl().__enter__().user = mock.Mock(username='bob')
            payload['object_kind'] = 'merge_request'
            payload['state'] = 'opened'

            res = common._process_gitlab_message(payload, webhooks, get_gl_instance=False)
            self.assertTrue(res)
            self.assertEqual(len(webhooks['merge_request'].call_args.args), 1)

    @mock.patch('webhook.common.validate_labels')
    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def _test_plabel_commands(self, mr_labels, new_labels, expected_cmd, is_prod, mock_validate):
        label_names = [label['name'] for label in new_labels]
        mock_validate.return_value = new_labels
        label_objects = []
        for label in self.PROJECT_LABELS:
            label_obj = mock.Mock(name=label['name'])
            label_obj.save = mock.Mock()
            for key in label:
                setattr(label_obj, key, label[key])
            label_objects.append(label_obj)

        gl_mergerequest = mock.Mock()
        gl_mergerequest.iid = 2
        gl_mergerequest.labels = [label['name'] for label in mr_labels]
        gl_mergerequest.notes.create = mock.Mock()

        gl_project = mock.Mock()
        gl_project.labels = MyLister(label_objects)
        gl_project.labels.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            label_ret = common.add_plabel_to_merge_request(gl_project, 2, label_names)

            # If there are more than 5 labels we should have used one API call to labels.list().
            labels_called_with_all = None
            labels_call_count = 0
            if len(new_labels) >= 5 and label_ret:
                labels_called_with_all = True
                labels_call_count = 1
            elif len(new_labels) <= 5 and label_ret:
                labels_called_with_all = False
                labels_call_count = expected_cmd.count('/label')
            self.assertEqual(gl_project.labels.called_with_all, labels_called_with_all)
            self.assertEqual(gl_project.labels.call_count, labels_call_count)

            # If label existed on the project, ensure it was edited if needed.
            for label in new_labels:
                pl_obj = \
                    next((pl for pl in label_objects if pl.name == label['name']), None)
                pl_dict = \
                    next((pl for pl in self.PROJECT_LABELS if pl['name'] == label['name']), None)
                if pl_obj and not (label.items() <= pl_dict.items()):
                    self.assertIn("Editing label %s on project." % label['name'],
                                  ' '.join(logs.output))
                    pl_obj.save.assert_called_once()
            # If new label didn't exist on the project, confirm it was added.
            call_list = []
            for label in new_labels:
                if not [plabel for plabel in self.PROJECT_LABELS
                        if plabel['name'] == label['name']]:
                    label_string = ("Creating label {'name': '%s', 'color': '%s',"
                                    " 'description': '%s'} on project.") \
                                    % (label['name'], label['color'], label['description'])
                    self.assertIn(label_string, ' '.join(logs.output))
                    call_list.append(mock.call(label))
            if call_list:
                gl_project.labels.create.assert_has_calls(call_list)
            else:
                gl_project.labels.create.assert_not_called()

        gl_project.mergerequests.get.assert_called_with(2)

        if expected_cmd:
            self.assertTrue(label_ret)
            gl_mergerequest.notes.create.assert_called_with({'body': expected_cmd})
        else:
            self.assertFalse(label_ret)
            gl_mergerequest.notes.create.assert_not_called()

    @mock.patch('webhook.common.validate_labels')
    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def _test_label_commands(self, mr_labels, new_labels, expected_cmd, is_prod, mock_validate,
                             remove_scoped=False):
        label_names = [label['name'] for label in new_labels]
        mock_validate.return_value = new_labels
        label_objects = []
        for label in self.PROJECT_LABELS:
            label_obj = mock.Mock(name=label['name'])
            label_obj.save = mock.Mock()
            for key in label:
                setattr(label_obj, key, label[key])
            label_objects.append(label_obj)

        gl_instance = mock.Mock()
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]
        gl_group = mock.Mock()
        gl_instance.groups.get.return_value = gl_group

        gl_mergerequest = mock.Mock()
        gl_mergerequest.iid = 2
        gl_mergerequest.labels = [label['name'] for label in mr_labels]
        gl_mergerequest.notes.create = mock.Mock()

        gl_project = mock.Mock()
        gl_project.labels = MyLister(label_objects)
        gl_project.labels.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")

        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            label_ret = common.add_label_to_merge_request(gl_instance, gl_project, 2, label_names,
                                                          remove_scoped)

            # If there are more than 5 labels we should have used one API call to labels.list().
            labels_called_with_all = None
            labels_call_count = 0
            if len(new_labels) >= 5 and label_ret:
                labels_called_with_all = True
                labels_call_count = 1
            elif len(new_labels) <= 5 and label_ret:
                labels_called_with_all = False
                labels_call_count = expected_cmd.count('/label')
            self.assertEqual(gl_project.labels.called_with_all, labels_called_with_all)
            self.assertEqual(gl_project.labels.call_count, labels_call_count)

            # If label existed on the project, ensure it was edited if needed.
            for label in new_labels:
                pl_obj = \
                    next((pl for pl in label_objects if pl.name == label['name']), None)
                pl_dict = \
                    next((pl for pl in self.PROJECT_LABELS if pl['name'] == label['name']), None)
                if pl_obj and not (label.items() <= pl_dict.items()):
                    self.assertIn("Editing label %s on group." % label['name'],
                                  ' '.join(logs.output))
                    pl_obj.save.assert_called_once()
            # If new label didn't exist on the project, confirm it was added.
            call_list = []
            for label in new_labels:
                if not [plabel for plabel in self.PROJECT_LABELS
                        if plabel['name'] == label['name']]:
                    label_string = ("Creating label {'name': '%s', 'color': '%s',"
                                    " 'description': '%s'} on group.") \
                                    % (label['name'], label['color'], label['description'])
                    self.assertIn(label_string, ' '.join(logs.output))
                    call_list.append(mock.call(label))
            if call_list:
                gl_group.labels.create.assert_has_calls(call_list)
            else:
                gl_group.labels.create.assert_not_called()

        gl_project.mergerequests.get.assert_called_with(2)

        if expected_cmd:
            self.assertTrue(label_ret)
            gl_mergerequest.notes.create.assert_called_with({'body': expected_cmd})
        else:
            self.assertFalse(label_ret)
            gl_mergerequest.notes.create.assert_not_called()

    @mock.patch('webhook.common._compute_mr_status_labels')
    @mock.patch('webhook.common._add_label_quick_actions', return_value=['/label "hello"'])
    @mock.patch('webhook.common._filter_mr_labels')
    def test_add_label_exception(self, mock_filter, mock_quickaction, mock_compute):
        mergerequest = mock.Mock()
        mergerequest.iid = 10
        instance = mock.Mock()
        project = mock.Mock()
        project.mergerequests.get.return_value = mergerequest
        mock_filter.return_value = ([], [])
        label_list = ['Acks::OK']

        # Catch "can't be blank" and move on.
        err_text = "400 Bad request - Note {:note=>[\"can't be blank\"]}"
        mergerequest.notes.create.side_effect = GitlabCreateError(error_message=err_text,
                                                                  response_code=400,
                                                                  response_body='')
        exception_raised = False
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            with mock.patch('cki_lib.misc.is_production', return_value=True):
                try:
                    result = common.add_label_to_merge_request(instance, project, mergerequest.iid,
                                                               label_list)
                except GitlabCreateError:
                    exception_raised = True
                self.assertTrue(result)
                self.assertFalse(exception_raised)
                self.assertIn("can't be blank", logs.output[-1])

        # For any other error let it bubble up.
        mergerequest.notes.create.side_effect = GitlabCreateError(error_message='oops',
                                                                  response_code=403,
                                                                  response_body='')
        exception_raised = False
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            try:
                common.add_label_to_merge_request(instance, project, mergerequest.iid, label_list)
            except GitlabCreateError:
                exception_raised = True
        self.assertTrue(exception_raised)

    def test_add_plabel_to_merge_request(self):
        # Add a project label which does not exist on the project.
        new_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                       'description': 'This MR has dependencies.'}]
        self._test_plabel_commands([], new_labels, '/label "Dependencies::abcdef012345"')

        # Add scoped Deps::OK::sha label
        new_labels = [{'name': 'Dependencies::OK::abcdef012345', 'color': '#123456',
                       'description': 'This MR has dependencies.'}]
        self._test_plabel_commands([], new_labels, '/label "Dependencies::OK::abcdef012345"')

        # Label already exists on MR, nothing to do.
        mr_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                      'description': 'This MR has dependencies.'}]
        new_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                       'description': 'This MR has dependencies.'}]
        self._test_plabel_commands(mr_labels, new_labels, None)

    def test_add_label_to_merge_request(self):
        # Add a label which already exists on the project.
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands([], new_labels, '/label "Acks::OK"')

        # Existing project label with new color.
        new_labels = [{'name': 'Acks::OK', 'color': '#123456'}]
        self._test_label_commands([], new_labels, '/label "Acks::OK"')

        # Label already exists on MR, nothing to do.
        mr_labels = [{'name': 'Acks::OK'},
                     {'name': 'CommitRefs::OK'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels, None)

        # Add a label which does not exist.
        new_labels = [{'name': 'NetSubsystem', 'color': '#123456',
                       'description': 'Net Subsystem label.'}]
        self._test_label_commands(mr_labels, new_labels, '/label "NetSubsystem"')

        # Add a label which triggers readyForMerge being added to the MR.
        mr_labels = [{'name': 'Bugzilla::OK'},
                     {'name': 'CKI::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK'},
                     {'name': 'Signoff::OK'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::OK"\n/label "readyForMerge"\n'
                                   '/unlabel "readyForQA"'))

        # Add a label which triggers readyForMerge being removed from the MR.
        mr_labels += [{'name': 'Acks::OK'}, {'name': 'readyForMerge'}]
        new_labels = [{'name': 'Acks::NeedsReview'}]
        expected_note = ['/label "Acks::NeedsReview"',
                         '/unlabel "Acks::OK"',
                         '/unlabel "readyForMerge"',
                         '/unlabel "readyForQA"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Add labels which trigger readyForQA.
        mr_labels = [{'name': 'Acks::NeedsReview'},
                     {'name': 'CKI::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK'},
                     {'name': 'Signoff::OK'}
                     ]
        new_labels = [{'name': 'Acks::OK'}, {'name': 'Bugzilla::NeedsTesting'}]
        expected_note = ['/label "Acks::OK"',
                         '/label "Bugzilla::NeedsTesting"',
                         '/unlabel "Acks::NeedsReview"',
                         '/label "readyForQA"',
                         '/unlabel "readyForMerge"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Add a double scoped label.
        new_labels = [{'name': 'BZ::123::OK', 'color': '#23456',
                       'description': 'Two scopes?'}]
        self._test_label_commands(mr_labels, new_labels, '/label "BZ::123::OK"')

        # Add two labels which both need to be created on the project. Use custom colors.
        new_labels = [{'name': 'Label 1', 'color': '#123456', 'description': 'description 1'},
                      {'name': 'Label 2', 'color': '#deadbe', 'description': 'description 2'}]
        self._test_label_commands(mr_labels, new_labels,
                                  '/label "Label 1"\n/label "Label 2"')

        # Add a NeedsTesting label.
        mr_labels = [{'name': 'Acks::OK'}, {'name': 'CommitRefs::OK'}]
        new_labels = [{'name': 'lnst::NeedsTesting'}]
        self._test_label_commands(mr_labels, new_labels, '/label "lnst::NeedsTesting"')

        # Add an lnst::OK label.
        mr_labels = [{'name': 'Acks::OK'}, {'name': 'CommitRefs::OK'},
                     {'name': 'lnst::NeedsTesting'}]
        new_labels = [{'name': 'lnst::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  '/label "lnst::OK"\n/unlabel "lnst::NeedsTesting"')

        # Change a single scoped label and ensure related double-scoped are not removed too.
        mr_labels += [{'name': 'Acks::net::OK'}]
        new_labels = [{'name': 'Acks::NeedsReview'}]
        expected_note = ['/label "Acks::NeedsReview"',
                         '/unlabel "Acks::OK"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Change a double scoped label and ensure only related double scoped are removed.
        mr_labels = [{'name': 'Acks::OK'}, {'name': 'Acks::net::OK'}]
        new_labels = [{'name': 'Acks::net::NeedsReview'}]
        expected_note = ['/label "Acks::net::NeedsReview"',
                         '/unlabel "Acks::net::OK"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Add a double scoped label with remove_scoped set.
        mr_labels = [{'name': 'CKI::OK'}, {'name': 'Acks::net::OK'}]
        new_labels = [{'name': 'CKI::Failed::test'}]
        expected_note = ['/label "CKI::Failed::test"',
                         '/unlabel "CKI::OK"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note),
                                  remove_scoped=True)

        # Add a lot of labels to trigger labels.list(all=True).
        new_labels = []
        count = 1
        while count <= 8:
            name = f"Label {count}"
            label = {'name': name, 'color': '#123456', 'description': name}
            new_labels.append(label)
            count += 1
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Label 1"\n/label "Label 2"\n/label "Label 3"'
                                   '\n/label "Label 4"\n/label "Label 5"\n/label "Label 6"'
                                   '\n/label "Label 7"\n/label "Label 8"'))

    def test_extract_dependencies(self):
        """Check for expected output."""
        # Test handling of None input message.
        bzs = common.extract_dependencies(None)
        self.assertEqual(bzs, [])

        message = ('This BZ has dependencies.\n'
                   'Bugzilla: https://bugzilla.redhat.com/1234567\n'
                   'Depends: https://bugzilla.redhat.com/22334455\n'
                   'Depends: https://bugzilla.redhat.com/33445566\n')
        bzs = common.extract_dependencies(message)
        self.assertEqual(bzs, ['22334455', '33445566'])

    def test_try_bugzilla_conn_no_key(self):
        """Check for negative return when BUGZILLA_API_KEY is not set."""
        self.assertFalse(common.try_bugzilla_conn())

    @mock.patch.dict(os.environ, {'BUGZILLA_API_KEY': 'mocked'})
    def test_try_bugzilla_conn_with_key(self):
        """Check for positive return when BUGZILLA_API_KEY is set."""
        with mock.patch('webhook.common.connect_bugzilla', return_value=True):
            self.assertTrue(common.try_bugzilla_conn())

    def test_connect_bugzilla(self):
        """Check for expected return value."""
        api_key = 'totally_fake_api_key'
        bzcon = mock.Mock()
        with mock.patch('bugzilla.Bugzilla', return_value=bzcon):
            self.assertEqual(common.connect_bugzilla(api_key), bzcon)

    @mock.patch('bugzilla.Bugzilla')
    def test_connect_bugzilla_exception(self, mocked_bugzilla):
        """Check ConnectionError exception generates logging."""
        api_key = 'totally_fake_api_key'
        mocked_bugzilla.side_effect = ConnectionError
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            self.assertFalse(common.connect_bugzilla(api_key))
            self.assertIn('Problem connecting to bugzilla server.', logs.output[-1])
        mocked_bugzilla.side_effect = PermissionError
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            self.assertFalse(common.connect_bugzilla(api_key))
            self.assertIn('Problem with file permissions', logs.output[-1])

    @mock.patch('webhook.common._edit_group_label')
    @mock.patch('webhook.common._edit_project_label')
    @mock.patch('webhook.common._match_label')
    def test_add_label_quick_actions(self, existing_label, plabel, glabel):
        """Check for the right label type (project or group) being evaluated."""
        mergerequest = mock.Mock()
        mergerequest.iid = 10
        instance = mock.Mock()
        project = mock.Mock()
        project.mergerequests.get.return_value = mergerequest
        label_list = [{'name': 'Acks::mm::OK'}]
        existing_label.return_value = ""
        output = common._add_label_quick_actions(instance, project, label_list)
        plabel.assert_not_called()
        glabel.assert_called_once()
        self.assertEqual(['/label "Acks::mm::OK"'], output)
        project.id = defs.ARK_PROJECT_ID
        common._add_label_quick_actions(instance, project, label_list)
        plabel.assert_called_once()
        self.assertEqual(['/label "Acks::mm::OK"'], output)

    def test_extract_all_bzs(self):
        """Check for expected output."""
        # Test handling of None input message.
        bzs, non_mr_bzs, dep_bzs = common.extract_all_bzs(None, [], [])
        self.assertEqual(bzs, [])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, [])

        message1 = 'Here is my perfect patch.\nBugzilla: https://bugzilla.redhat.com/18123456'
        bzs, non_mr_bzs, dep_bzs = common.extract_all_bzs(message1, [], [])
        self.assertEqual(bzs, ['18123456'])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, [])
        bzs, non_mr_bzs, dep_bzs = common.extract_all_bzs(message1, [], ['18123456'])
        self.assertEqual(bzs, [])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, ['18123456'])
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            bzs, non_mr_bzs, dep_bzs = common.extract_all_bzs(message1, ['12345678'], [])
            self.assertIn('Bugzilla: 18123456 not listed in MR description.', logs.output[-1])
            self.assertEqual(bzs, [])
            self.assertEqual(non_mr_bzs, ['18123456'])
            self.assertEqual(dep_bzs, [])

    def test_parse_gl_project_path(self):
        url1 = 'http://www.gitlab.com/mycoolproject/-/merge_requests/12345'
        url2 = 'http://www.gitlab.com/group/subgroup/project/-/pipelines/35435747'
        url3 = 'https://gitlab.com/this/is/a/test/-/issues/'

        self.assertEqual('mycoolproject', common.parse_gl_project_path(url1))
        self.assertEqual('group/subgroup/project', common.parse_gl_project_path(url2))
        self.assertEqual('this/is/a/test', common.parse_gl_project_path(url3))

    def test_bugs_to_process(self):
        old_description = ('Bugzilla: https://bugzilla.redhat.com/1234567\n'
                           'Bugzilla: https://bugzilla.redhat.com/2345678\n'
                           'Bugzilla: https://bugzilla.redhat.com/3456789\n'
                           'Bugzilla: https://bugzilla.redhat.com/8765432')
        description = ('Bugzilla: https://bugzilla.redhat.com/1234567\n'
                       'Bugzilla: https://bugzilla.redhat.com/2345678\n'
                       'Bugzilla: https://bugzilla.redhat.com/3456789')
        changes = {'description': {'previous': old_description,
                                   'current': description}
                   }

        # Link all in description, unlink 8765432.
        result = common.bugs_to_process(description, 'open', changes)
        self.assertEqual(set(result['link']), {'1234567', '2345678', '3456789'})
        self.assertEqual(set(result['unlink']), {'8765432'})

        # If the action is close then just unlink everything in the current description.
        result = common.bugs_to_process(description, 'close', {})
        self.assertEqual(result['unlink'], {'1234567', '2345678', '3456789'})
        self.assertEqual(result['link'], set())

    def test_get_mr(self):
        gl_project = mock.Mock(id=234567)
        gl_mr = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mr

        # No error, gl_mr object is returned.
        exception_raised = False
        try:
            result = common.get_mr(gl_project, 123)
        except:  # noqa: E722
            exception_raised = True
        gl_project.mergerequests.get.assert_called_with(123)
        self.assertEqual(result, gl_mr)
        self.assertFalse(exception_raised)

        # GitlabGetError code 404, None returned.
        exception_raised = False
        gl_project.mergerequests.get.reset_mock(return_value=True)
        gl_project.mergerequests.get.side_effect = GitlabGetError(response_code=404,
                                                                  error_message='oh no!')
        with self.assertLogs('cki.webhook.common', level='WARNING') as logs:
            try:
                result = common.get_mr(gl_project, 456)
            except GitlabGetError as err:
                if err.response_code == 404:
                    exception_raised = True
            gl_project.mergerequests.get.assert_called_with(456)
            self.assertIn('MR 456 does not exist in project 234567 (404).', logs.output[-1])
            self.assertEqual(result, None)
            self.assertFalse(exception_raised)

        # An unhandled exception so blow up.
        exception_raised = False
        gl_project.mergerequests.get.reset_mock()
        gl_project.mergerequests.get.side_effect = GitlabGetError(response_code=502,
                                                                  error_message='oh no!')
        try:
            common.get_mr(gl_project, 789)
        except GitlabGetError as err:
            if err.response_code == 502:
                exception_raised = True
        gl_project.mergerequests.get.assert_called_with(789)
        self.assertTrue(exception_raised)

    def test_get_pipeline(self):
        gl_project = mock.Mock(id=234567)
        gl_pipeline = mock.Mock()
        gl_project.pipelines.get.return_value = gl_pipeline

        # No error, gl_pipeline object is returned.
        exception_raised = False
        try:
            result = common.get_pipeline(gl_project, 123)
        except:  # noqa: E722
            exception_raised = True
        gl_project.pipelines.get.assert_called_with(123)
        self.assertEqual(result, gl_pipeline)
        self.assertFalse(exception_raised)

        # GitlabGetError code 404, None returned.
        exception_raised = False
        gl_project.pipelines.get.reset_mock(return_value=True)
        gl_project.pipelines.get.side_effect = GitlabGetError(response_code=404,
                                                              error_message='oh no!')
        with self.assertLogs('cki.webhook.common', level='WARNING') as logs:
            try:
                result = common.get_pipeline(gl_project, 456)
            except GitlabGetError as err:
                if err.response_code == 404:
                    exception_raised = True
            gl_project.pipelines.get.assert_called_with(456)
            self.assertIn('Pipeline 456 does not exist in project 234567 (404).', logs.output[-1])
            self.assertEqual(result, None)
            self.assertFalse(exception_raised)

        # An unhandled exception so blow up.
        exception_raised = False
        gl_project.pipelines.get.reset_mock()
        gl_project.pipelines.get.side_effect = GitlabGetError(response_code=502,
                                                              error_message='oh no!')
        try:
            common.get_pipeline(gl_project, 789)
        except GitlabGetError as err:
            if err.response_code == 502:
                exception_raised = True
        gl_project.pipelines.get.assert_called_with(789)
        self.assertTrue(exception_raised)

    @mock.patch('webhook.common.get_session')
    @mock.patch('webhook.common.get_token')
    @mock.patch('webhook.common.Client')
    def test_gl_graphql_client(self, mock_client, mock_get_token, mock_get_session):
        mock_get_session.return_value = mock.Mock(headers={})
        mock_get_token.return_value = 'fake_token'
        result = common.gl_graphql_client()
        transport = mock_client.call_args.kwargs['transport']
        self.assertEqual(transport.__class__.__name__, '_CkiRequestsHTTPTransport')
        self.assertEqual(transport.session, mock_get_session())
        self.assertEqual(transport.session.headers, {'Authorization': 'Bearer fake_token'})
        self.assertEqual(result, mock_client())

    def test_draft_status(self):
        merge_dict = {'object_kind': 'merge_request',
                      'project': {'id': 1,
                                  'web_url': 'https://web.url/g/p'},
                      'object_attributes': {'target_branch': 'main',
                                            'iid': 2,
                                            'work_in_progress': False},
                      'state': 'opened',
                      'action': 'open',
                      'labels': []
                      }

        # Not a Draft, MR message changes do not include 'title'.
        merge_dict['object_attributes']['work_in_progress'] = False
        merge_dict['changes'] = {}
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertFalse(is_draft)
        self.assertFalse(changed)

        # Not a draft and Titles don't mention Draft:.
        merge_dict['changes'] = {'title': {'previous': 'My awesome MR',
                                           'current': 'My cool MR'}
                                 }
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertFalse(is_draft)
        self.assertFalse(changed)

        # No 'previous' title member.
        merge_dict['changes'] = {'title': {'current': 'My cool MR'}}
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertFalse(is_draft)
        self.assertFalse(changed)

        # It's not a Draft any more!
        merge_dict['changes'] = {'title': {'previous': '[Draft] My cool MR',
                                           'current': 'My cool MR'}
                                 }
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertFalse(is_draft)
        self.assertTrue(changed)

        # I turned into a Draft.
        merge_dict['object_attributes']['work_in_progress'] = True
        merge_dict['changes'] = {'title': {'previous': 'My cool MR',
                                           'current': 'Draft: My cool MR'}
                                 }
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertTrue(is_draft)
        self.assertTrue(changed)

    @mock.patch('webhook.common.run')
    def test__grep_for_config(self, mock_run):
        # No results
        mock_run.return_value = mock.Mock(returncode=1)
        results = common._grep_for_config('CONFIG_FUN', '/linus')
        self.assertEqual(results, [])
        mock_run.return_value.check_returncode.assert_not_called()

        # Found something
        mock_run.return_value.returncode = 0
        mock_run.return_value.stdout = '\n/linus/arch/Kconfig\n/linus/arch/x86_64/Kconfig'
        results = common._grep_for_config('CONFIG_FUN', '/linus')
        self.assertEqual(results, ['arch/Kconfig', 'arch/x86_64/Kconfig'])
        mock_run.return_value.check_returncode.assert_not_called()

        # Called check_returncode()
        mock_run.return_value.returncode = 2
        results = common._grep_for_config('CONFIG_FUN', '/linus')
        mock_run.return_value.check_returncode.assert_called_once()

    @mock.patch('webhook.common._grep_for_config')
    def test_process_config_items(self, mock_grep):
        # Input nothing, return nothing
        path_list = []
        results = common.process_config_items('/linus', path_list)
        self.assertEqual(results, set())

        # A hit
        path_list = ['redhat/configs/common/generic/x86/x86_64/CONFIG_TURBO', 'README']
        mock_grep.return_value = ['arch/x86_64/Kconfig']
        results = common.process_config_items('/linus', path_list)
        self.assertEqual(results, set(['arch/x86_64/Kconfig']))

    def test_load_yaml_data(self):
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            result = common.load_yaml_data('bad_map_file.yml')
            self.assertEqual(result, None)
            self.assertIn("No such file or directory: 'bad_map_file.yml'", logs.output[-1])

        with mock.patch('builtins.open', mock.mock_open(read_data=self.FILE_CONTENTS)):
            result = common.load_yaml_data('map_file.yml')
            self.assertEqual(result, self.MAPPINGS)

    def test_match_single_label(self):
        # No match
        results = common.match_single_label('readyForMerge', self.YAML_LABELS)
        self.assertEqual(results, None)

        # Basic match
        results = common.match_single_label('Acks::OK', self.YAML_LABELS)
        self.assertEqual(results, self.YAML_LABELS[0])

        # Regex match
        results = common.match_single_label('Subsystem:Cookies', self.YAML_LABELS)
        expected = {'name': 'Subsystem:Cookies', 'color': '#778899', 'description': 'hi Cookies'}
        self.assertEqual(results, expected)

    @mock.patch('webhook.common.load_yaml_data')
    def test_validate_labels(self, mock_load_yaml):
        # No data generates a runtime error
        mock_load_yaml.return_value = None
        raised = False
        try:
            results = common.validate_labels(['hi', 'there'], 'utils/labels.yaml')
        except RuntimeError:
            raised = True
        mock_load_yaml.assert_called_with('utils/labels.yaml')
        self.assertTrue(raised)

        # Find some labels
        mock_load_yaml.return_value = {'labels': self.YAML_LABELS}
        results = common.validate_labels(['Acks::OK', 'Subsystem:net'], 'yaml_path')
        expected = [{'name': 'Acks::OK', 'color': '#428BCA', 'description': 'all good'},
                    {'name': 'Subsystem:net', 'color': '#778899', 'description': 'hi net'}
                    ]
        mock_load_yaml.assert_called_with('yaml_path')
        self.assertEqual(results, expected)

        # Can't find it!
        raised = False
        try:
            results = common.validate_labels(['Acks::OK', 'Frogs::OK', 'Subsystem:net'], 'yaml')
        except RuntimeError as err:
            raised = True
            self.assertIn("unknown: ['Frogs::OK']", err.args[0])
        mock_load_yaml.assert_called_with('yaml')
        self.assertTrue(raised)
