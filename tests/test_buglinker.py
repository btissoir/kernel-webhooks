"""Webhook interaction tests."""
import copy
import os
from unittest import TestCase
from unittest import mock
from xmlrpc.client import Fault

from bugzilla import BugzillaError
from gitlab.exceptions import GitlabGetError

from webhook import buglinker
from webhook import defs


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestBuglinker(TestCase):
    """ Test Webhook class."""

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'web_url': 'https://web.url/g/p',
                                 'path_with_namespace': 'group/project'
                                 },
                     'object_attributes': {'target_branch': 'main',
                                           'iid': 2,
                                           'state': 'opened',
                                           'action': 'update'
                                           },
                     'labels': [],
                     'changes': {}
                     }

    PAYLOAD_PIPELINE = {'object_kind': 'pipeline',
                        'project': {'id': 1,
                                    'path_with_namespace': 'group/project'},
                        'object_attributes': {'id': 22334455,
                                              'status': 'success'},
                        'merge_request': {'iid': 10,
                                          'title': 'a test merge request'}
                        }

    BZ_RESULTS = [{'id': 1234567,
                   'product': 'Red Hat Enterprise Linux 8',
                   'version': '8.2',
                   'summary': 'CVE-2020-02315 kernel: a bad one',
                   'external_bugs': [{'ext_bz_bug_id': '111222',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'redhat/rhel/8.y/kernel/-/merge_requests/10',
                                      'type': {'description': 'Gitlab',
                                               'url': 'https://gitlab.com/'
                                               }
                                      }]
                   },
                  {'id': 8675309,
                   'product': 'Red Hat Enterprise Linux 8',
                   'version': '8.2',
                   'summary': 'CVE-2020-1234 kernel-rt: another one',
                   'external_bugs': [{'ext_bz_bug_id': '11223344',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'group/project/-/merge_requests/321',
                                      'type': {'description': 'Gitlab',
                                               'url': 'https://gitlab.com/'
                                               }
                                      }]
                   },
                  {'id': 66442200,
                   'product': 'Red Hat Enterprise Linux 8',
                   'version': '8.2',
                   'summary': 'A kernel bug?',
                   'external_bugs': []
                   }]

    def test_get_bugs(self):
        bzcon = mock.Mock()

        # No results.
        bzcon.getbugs.return_value = None
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            results = buglinker.get_bugs(bzcon, [123])
            self.assertEqual(results, None)
            self.assertIn('getbugs() returned an empty list for these bugs: [123]', logs.output[-1])

        # Results.
        bzcon.getbugs.return_value = [456]
        results = buglinker.get_bugs(bzcon, [123])
        self.assertEqual(results, bzcon.getbugs.return_value)

        # bzcon raises an exception.
        bzcon.getbugs.side_effect = BugzillaError('oh no!')
        with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
            exception_hit = False
            try:
                buglinker.get_bugs(bzcon, ['burp'])
            except BugzillaError:
                exception_hit = True
            self.assertFalse(exception_hit)
            self.assertIn('Error getting bugs.', logs.output[-1])

    @mock.patch('webhook.buglinker.unlink_mr_from_bzs')
    @mock.patch('webhook.buglinker.link_mr_to_bzs')
    @mock.patch('webhook.buglinker.get_bugs')
    def test_update_bugzilla(self, mock_get_bugs, mock_link, mock_unlink):
        """Check for expected calls."""
        namespace = 'redhat/rhel/8.y/kernel'
        mr_id = 10
        ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        bzcon = mock.Mock()

        # Ignore non-numeric bugs from the input bug list.
        bugs = {'link': ['INTERNAL'], 'unlink': []}

        buglinker.update_bugzilla(bugs, namespace, mr_id, bzcon)
        mock_link.assert_not_called()
        mock_unlink.assert_not_called()

        # Call add_mr_to_bz() with the expected parameters.
        bugs = {'link': ['252626', '739272', '1544362', 'INTERNAL'],
                'unlink': ['1234567']
                }
        bz0 = mock.Mock()
        bz1 = mock.Mock()
        bz2 = mock.Mock()
        mock_get_bugs.return_value = [bz0, bz1, bz2]

        buglinker.update_bugzilla(bugs, namespace, mr_id, bzcon)
        mock_get_bugs.assert_called_with(bzcon, ['252626', '739272', '1544362'])
        mock_link.assert_called_once_with(mock_get_bugs.return_value, ext_bz_bug_id, bzcon)
        mock_unlink.assert_called_once_with(['1234567'], ext_bz_bug_id, bzcon)

    def test_bz_is_linked_to_mr(self):
        """Check for expected return values."""
        ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        bz0 = mock.Mock(id=self.BZ_RESULTS[0]['id'],
                        external_bugs=self.BZ_RESULTS[0]['external_bugs'])
        bz1 = mock.Mock(id=self.BZ_RESULTS[1]['id'],
                        external_bugs=self.BZ_RESULTS[1]['external_bugs'])
        bz2 = mock.Mock(id=self.BZ_RESULTS[2]['id'],
                        external_bugs=self.BZ_RESULTS[2]['external_bugs'])
        self.assertTrue(buglinker.bz_is_linked_to_mr(bz0, ext_bz_bug_id))
        self.assertFalse(buglinker.bz_is_linked_to_mr(bz1, ext_bz_bug_id))
        self.assertFalse(buglinker.bz_is_linked_to_mr(bz2, ext_bz_bug_id))

    def test_parse_cve_from_summary(self):
        """Check for expected return values."""
        summary = 'CVE-2020-02345 a bad problem.'
        self.assertTrue(buglinker.parse_cve_from_summary(summary))
        summary = 'kernel bug (again)'
        self.assertEqual(buglinker.parse_cve_from_summary(summary), None)
        summary = 'CVE-123-abcd kernel: the worst problem.'
        self.assertEqual(buglinker.parse_cve_from_summary(summary), None)

    @mock.patch('webhook.buglinker.get_bugs')
    def test_get_rt_cve_bugs(self, mock_get_bugs):
        bz0 = mock.Mock(id=11223344, summary='CVE-2020-12345 kernel: a problem to fix.')
        bz1 = mock.Mock(id=22334455, summary='kernel: another problem to fix.')
        bz2 = mock.Mock(id=33445566, summary='CVE-2021-4535 kernel: again a problem to fix.')
        bz3 = mock.Mock(id=44556677, summary='CVE-1996-01163 kernel-rt: how did we miss this?')
        bz4 = mock.Mock(id=55667788, summary='kernel stuff')
        bzcon = mock.Mock()

        # No CVE bugs.
        mock_get_bugs.return_value = [bz1, bz4]
        result = buglinker.get_rt_cve_bugs(bzcon, [bz1.id, bz4.id])
        self.assertEqual(result, None)
        bzcon.query.assert_not_called()

        # Three CVE bugs.
        mock_get_bugs.return_value = [bz0, bz1, bz2, bz3, bz4]
        result = buglinker.get_rt_cve_bugs(bzcon, [bz0.id, bz1.id, bz2.id, bz3.id, bz4.id])
        self.assertEqual(result, bzcon.query.return_value)
        self.assertEqual(sorted(bzcon.query.call_args.args[0]['v1']),
                         ['CVE-1996-01163', 'CVE-2020-12345', 'CVE-2021-4535'])

        # bzcon raises an exception.
        bzcon.query.side_effect = BugzillaError('oh no!')
        with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
            exception_hit = False
            try:
                buglinker.get_rt_cve_bugs(bzcon, [bz0.id, bz1.id, bz2.id, bz3.id, bz4.id])
            except BugzillaError:
                exception_hit = True
            self.assertFalse(exception_hit)
            self.assertIn('Error querying bugzilla.', logs.output[-1])

    @mock.patch('cki_lib.misc.is_production')
    def test_unlink_mr_from_bzs(self, mock_production):
        """Check the call to bzcon.add_external_tracker() has the expected input."""
        ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        bzcon = mock.Mock()

        # Not production. Don't do anything.
        mock_production.return_value = False
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            buglinker.unlink_mr_from_bzs([1234567], ext_bz_bug_id, bzcon)
            self.assertIn(f'Unlinking {ext_bz_bug_id} from BZ1234567.', logs.output[-1])
            bzcon.remove_external_tracker.assert_not_called()

        # Bugzilla Exception.
        mock_production.return_value = True
        bzcon.remove_external_tracker.side_effect = BugzillaError('oh no!')
        with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
            exception_hit = False
            try:
                buglinker.unlink_mr_from_bzs([1234567, 2345678], ext_bz_bug_id, bzcon)
            except BugzillaError:
                exception_hit = True
            self.assertFalse(exception_hit)

        # Known xmlrpc Fault.
        bzcon.remove_external_tracker.side_effect = Fault(faultCode=1006,
                                                          faultString='no link to remove.')
        with self.assertLogs('cki.webhook.buglinker', level='WARNING') as logs:
            exception_hit = False
            try:
                buglinker.unlink_mr_from_bzs([1234567, 2345678], ext_bz_bug_id, bzcon)
            except Fault:
                exception_hit = True
            self.assertFalse(exception_hit)
            self.assertIn('xmlrpc fault 1006:', logs.output[-1])

        # Known xmlrpc Fault.
        bzcon.remove_external_tracker.side_effect = Fault(faultCode=123, faultString='oh no!')
        exception_hit = False
        try:
            buglinker.unlink_mr_from_bzs([1234567, 2345678], ext_bz_bug_id, bzcon)
        except Fault:
            exception_hit = True
        self.assertTrue(exception_hit)

        # Go Go Go.
        bzcon.remove_external_tracker.reset_mock(side_effect=True)

        buglinker.unlink_mr_from_bzs([1234567, 2345678], ext_bz_bug_id, bzcon)
        self.assertEqual(bzcon.remove_external_tracker.call_count, 2)

    @mock.patch.dict(os.environ, {'BUGZILLA_API_KEY': 'mocked'})
    def test_link_mr_to_bzs(self):
        """Check the call to bzcon.add_external_tracker() has the expected input."""
        ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        ext_type_url = defs.EXT_TYPE_URL
        bz0 = mock.Mock(id=self.BZ_RESULTS[0]['id'],
                        product='Red Hat Enterprise Linux 8',
                        component='kernel',
                        external_bugs=self.BZ_RESULTS[0]['external_bugs'])
        bz1 = mock.Mock(id=self.BZ_RESULTS[1]['id'],
                        product='Red Hat Enterprise Linux 8',
                        component='kernel-rt',
                        external_bugs=self.BZ_RESULTS[1]['external_bugs'])
        bz2 = mock.Mock(id=self.BZ_RESULTS[2]['id'],
                        product='Red Hat Enterprise Linux 8',
                        component='kernel',
                        external_bugs=self.BZ_RESULTS[2]['external_bugs'])
        bzcon = mock.Mock()

        # Early return due to no untracked bugs
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            buglinker.link_mr_to_bzs([bz0], ext_bz_bug_id, bzcon)
            self.assertIn(f'All bugs have an existing link to {ext_bz_bug_id}.', logs.output[-1])

        # bzcon raises an exception.
        bzcon.add_external_tracker.side_effect = BugzillaError('oh no!')
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
                exception_hit = False
                try:
                    buglinker.link_mr_to_bzs([bz0, bz1, bz2], ext_bz_bug_id, bzcon)
                except BugzillaError:
                    exception_hit = True
                self.assertFalse(exception_hit)
                self.assertIn(f'Problem adding tracker {ext_bz_bug_id} to BZs.', logs.output[-1])
                bzcon.add_external_tracker.assert_called_with([bz1.id, bz2.id],
                                                              ext_type_url=ext_type_url,
                                                              ext_bz_bug_id=ext_bz_bug_id)

        # Successful call to add_external_tracker()
        bzcon.add_external_tracker.side_effect = None
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            buglinker.link_mr_to_bzs([bz0, bz1, bz2], ext_bz_bug_id, bzcon)
        bzcon.add_external_tracker.assert_called_with([bz1.id, bz2.id],
                                                      ext_type_url=ext_type_url,
                                                      ext_bz_bug_id=ext_bz_bug_id)

    def test_get_artifact(self):
        mock_job = mock.Mock(id=1234567)

        # GitlabGetError error.
        mock_job.artifact.side_effect = GitlabGetError('oh no!')
        with self.assertLogs('cki.webhook.buglinker', level='WARNING') as logs:
            exception_hit = False
            try:
                buglinker.get_artifact(mock_job, 'rc')
            except GitlabGetError:
                exception_hit = True
            self.assertFalse(exception_hit)
            self.assertIn('Error getting path for job #1234567: rc', logs.output[-1])

        # No error.
        mock_job.artifact.side_effect = False
        mock_job.artifact = mock.Mock(return_value='A cool file')

        result = buglinker.get_artifact(mock_job, 'rc')
        self.assertEqual(result, 'A cool file')

    @mock.patch('webhook.buglinker.comment_already_posted')
    @mock.patch('webhook.buglinker.get_rt_cve_bugs')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_post_to_bugs(self, mock_rt_cve_bugs, mock_comment_check):
        bzcon = mock.Mock()
        bz1 = mock.Mock(id=11223344)
        bz2 = mock.Mock(id=22334455)
        bz3 = mock.Mock(id=33445566)
        bz4 = mock.Mock(id=44556677)

        # No bug objects, nothing to return.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            bug_list = [bz1.id, bz2.id]
            bzcon.getbugs.return_value = None

            result = buglinker.post_to_bugs(bug_list, 'text', 'pipeline_url', 'trigger', bzcon)
            self.assertEqual(result, None)
            bzcon.update_bugs.assert_not_called()

        # Comment already posted to all bugs so don't return
        # anything (bugs should already be linked).
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            bug_list = [bz1.id, bz2.id]
            bzcon.getbugs.return_value = [bz1, bz2]
            mock_comment_check.return_value = True

            result = buglinker.post_to_bugs(bug_list, 'text', 'pipeline_url', 'trigger', bzcon)
            self.assertEqual(result, None)
            self.assertIn('Pipeline results have already been posted to all relevant bugs.',
                          logs.output[-1])
            self.assertEqual(mock_comment_check.call_count, 2)
            bzcon.update_bugs.assert_not_called()

        # Succesful comment, one filtered bug.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_comment_check.return_value = False
            mock_comment_check.side_effect = [False, True]

            buglinker.post_to_bugs(bug_list, 'text', 'pipeline_url', 'trigger', bzcon)
            bzcon.build_update.assert_called_with(comment='text', comment_private=True)
            bzcon.update_bugs.assert_called_with([bz1.id], bzcon.build_update())
            self.assertIn('Posted comment to bugs: [11223344]', logs.output[-1])

        # Succesful comment on RT pipeline, one filtered bug.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            pipe_name = defs.KERNEL_RT_PIPELINES[0]
            mock_comment_check.return_value = False
            mock_comment_check.side_effect = [False, True]
            mock_rt_cve_bugs.return_value = [bz3, bz4]

            buglinker.post_to_bugs(bug_list, 'text', 'pipeline_url', pipe_name, bzcon)
            bzcon.update_bugs.assert_called_with([bz3.id], bzcon.build_update())
            self.assertIn('Posted comment to bugs: [33445566]', logs.output[-1])

        # Exception.
        with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
            mock_comment_check.side_effect = None
            bzcon.update_bugs.side_effect = BugzillaError(message='oops')

            buglinker.post_to_bugs(bug_list, 'text', 'pipeline_url', 'trigger', bzcon)
            bzcon.build_update.assert_called_with(comment='text', comment_private=True)
            bzcon.update_bugs.assert_called_with([bz1.id, bz2.id], bzcon.build_update())
            self.assertIn('Error posting comments to bugs: [11223344, 22334455]', logs.output[-1])

    def test_comment_already_posted(self):
        pipeline_url = 'https://gitlab.com/project/-/pipelines/12345'
        pipeline_text = f'Pipeline: {pipeline_url}'
        comment1 = {'creator': 'user@redhat.com', 'text': 'cool comment', 'count': 1}
        comment2 = {'creator': defs.KERNEL_BZ_BOT,
                    'text': f'hey there\n{pipeline_text}\nthanks', 'count': 2}
        bug = mock.Mock
        bug.id = 678910

        # comment2 should return True.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            bug.getcomments = mock.Mock(return_value=[comment1, comment2])
            self.assertTrue(buglinker.comment_already_posted(bug, pipeline_url))
            self.assertIn('Excluding bug 678910 as pipeline was already posted in comment 2.',
                          logs.output[-1])

        # comment1 is False.
        bug.getcomments = mock.Mock(return_value=[comment1])
        self.assertFalse(buglinker.comment_already_posted(bug, pipeline_url))

    @mock.patch('webhook.buglinker.get_instance')
    @mock.patch('webhook.buglinker.link_mr_to_bzs')
    @mock.patch('webhook.common.try_bugzilla_conn')
    @mock.patch('webhook.buglinker.post_to_bugs')
    def test_process_pipeline(self, mock_post, mock_bz, mock_add_mr, mock_getinstance):
        mock_gl = mock.Mock()
        mock_getinstance.return_value = mock_gl

        # Pipeline event without an MR.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            payload = copy.deepcopy(self.PAYLOAD_PIPELINE)
            payload['merge_request'] = None
            msg = mock.Mock(payload=payload)

            buglinker.process_pipeline(msg)
            self.assertIn('Pipeline 22334455 is not associated with an MR, ignoring.',
                          logs.output[-1])
            mock_post.assert_not_called()

        # MR event, MR is a WIP.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            merge_msg = copy.deepcopy(self.PAYLOAD_MERGE)
            msg = mock.Mock(payload=merge_msg)
            mock_project = mock.Mock(path_with_namespace='project')
            mock_gl.projects.get.return_value = mock_project
            mock_mr = mock.Mock(iid=2, description='', work_in_progress=True)
            mock_project.mergerequests.get.return_value = mock_mr

            buglinker.process_pipeline(msg)
            self.assertIn('MR 2 is marked work in progress, ignoring.', logs.output[-1])
            mock_post.assert_not_called()

        # Pipeline event, MR head_pipeline member is None.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            msg = mock.Mock(payload=copy.deepcopy(self.PAYLOAD_PIPELINE))
            mock_project = mock.Mock(path_with_namespace='project')
            mock_gl.projects.get.return_value = mock_project
            mock_mr = mock.Mock(iid=10, description='', work_in_progress=False,
                                labels=['readyForQA'], head_pipeline=None)
            mock_project.mergerequests.get.return_value = mock_mr

            buglinker.process_pipeline(msg)
            self.assertIn("MR 10 has not triggered any pipelines? head_pipeline is None.",
                          logs.output[-1])
            mock_post.assert_not_called()

        # MR event, pipeline is in the wrong namespace.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            merge_msg = copy.deepcopy(self.PAYLOAD_MERGE)
            msg = mock.Mock(payload=merge_msg)
            mock_project = mock.Mock(path_with_namespace='project')
            mock_gl.projects.get.return_value = mock_project
            head_pipeline = {'id': 22334455, 'status': 'running',
                             'web_url': 'https://gitlab.com/ptalbert/rhel-8/-/pipelines/22334455'}
            mock_mr = mock.Mock(iid=2, description='', work_in_progress=False,
                                labels=['readyForQA'], head_pipeline=head_pipeline)
            mock_project.mergerequests.get.return_value = mock_mr

            buglinker.process_pipeline(msg)
            self.assertIn('MR 2 head pipeline #22334455 is not in the Red Hat namespace',
                          logs.output[-1])
            mock_post.assert_not_called()

        # MR does not have any bugs listed.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            merge_msg = copy.deepcopy(self.PAYLOAD_MERGE)
            msg = mock.Mock(payload=merge_msg)
            mock_project = mock.Mock(path_with_namespace='project')
            mock_gl.projects.get.return_value = mock_project
            head_pipeline = {'id': 22334455, 'status': 'success',
                             'web_url': 'https://gitlab.com/redhat/rhel-8/-/pipelines/22334455'}
            mock_mr = mock.Mock(iid=2, description='', work_in_progress=False,
                                labels=['readyForQA'], head_pipeline=head_pipeline)
            mock_project.mergerequests.get.return_value = mock_mr

            buglinker.process_pipeline(msg)
            self.assertIn('No bugs found in MR 2 description.', logs.output[-1])
            mock_post.assert_not_called()

        # Unknown bridge job name.
        with self.assertLogs('cki.webhook.buglinker', level='DEBUG') as logs:
            description = ('Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=123456\n'
                           'Depends: https://bugzilla.redhat.com/show_bug.cgi?id=56789')
            head_pipeline = {'id': 22334455, 'status': 'success',
                             'web_url': 'https://gitlab.com/redhat/rhel-8/-/pipelines/22334455'}
            mock_mr = mock.Mock(iid=10, description=description, title="A Cool MR",
                                web_url="https://gitlab.com/project/-/merge_requests/10",
                                work_in_progress=False, labels=['readyForQA'],
                                head_pipeline=head_pipeline)
            mock_project.mergerequests.get.return_value = mock_mr
            bridge = mock.Mock()
            bridge.name = 'unexpected_pipeline'
            pipeline = mock.Mock(id=22334455, status='success')
            pipeline.bridges.list.return_value = [bridge]
            mock_project.pipelines.get.return_value = pipeline
            mock_gl.projects.get.return_value = mock_project

            buglinker.process_pipeline(msg)
            self.assertIn('Unrecognized bridge job: unexpected_pipeline', logs.output[-2])
            self.assertIn('No job data.', logs.output[-1])
            mock_post.assert_not_called()

        # Bridge job downstream pipeline is None
        with self.assertLogs('cki.webhook.buglinker', level='DEBUG') as logs:
            description = ('Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=123456\n'
                           'Depends: https://bugzilla.redhat.com/show_bug.cgi?id=56789')
            head_pipeline = {'id': 22334455, 'status': 'success',
                             'web_url': 'https://gitlab.com/redhat/rhel-8/-/pipelines/22334455'}
            mock_mr = mock.Mock(iid=10, description=description, title="A Cool MR",
                                web_url="https://gitlab.com/project/-/merge_requests/10",
                                work_in_progress=False, labels=['readyForQA'],
                                head_pipeline=head_pipeline)
            mock_project.mergerequests.get.return_value = mock_mr
            bridge = mock.Mock()
            bridge.name = defs.KERNEL_PIPELINES[0]
            bridge.downstream_pipeline = None
            pipeline = mock.Mock(id=22334455, status='success')
            pipeline.bridges.list.return_value = [bridge]
            mock_project.pipelines.get.return_value = pipeline
            mock_gl.projects.get.side_effect = [mock_project]

            buglinker.process_pipeline(msg)
            bridge_str = f'Bridge job {bridge.name} does not have a downstream pipeline.'
            self.assertIn(bridge_str, logs.output[-2])
            self.assertIn('No job data.', logs.output[-1])
            mock_post.assert_not_called()

        # No downstream pipeline?
        with self.assertLogs('cki.webhook.buglinker', level='DEBUG') as logs:
            description = ('Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=123456\n'
                           'Depends: https://bugzilla.redhat.com/show_bug.cgi?id=56789')
            head_pipeline = {'id': 22334455, 'status': 'success',
                             'web_url': 'https://gitlab.com/redhat/rhel-8/-/pipelines/22334455'}
            mock_mr = mock.Mock(iid=10, description=description, title="A Cool MR",
                                web_url="https://gitlab.com/project/-/merge_requests/10",
                                work_in_progress=False, labels=['readyForQA'],
                                head_pipeline=head_pipeline)
            mock_project.mergerequests.get.return_value = mock_mr
            bridge = mock.Mock()
            bridge.name = defs.KERNEL_PIPELINES[0]
            bridge.downstream_pipeline = {'web_url': 'bad url'}
            pipeline = mock.Mock(id=22334455, status='success')
            pipeline.bridges.list.return_value = [bridge]
            mock_project.pipelines.get.return_value = pipeline
            mock_dsproject = mock.Mock()
            mock_dsproject.pipelines.get.return_value = None
            mock_gl.projects.get.side_effect = [mock_project, mock_dsproject]

            buglinker.process_pipeline(msg)
            bridge_str = f'Could not get downstream pipeline for bridge job {bridge.name}.'
            self.assertIn(bridge_str, logs.output[-2])
            self.assertIn('No job data.', logs.output[-1])
            mock_post.assert_not_called()

        # Downstream pipeline status is not 'success' or 'failed'.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            description = ('Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=123456\n'
                           'Depends: https://bugzilla.redhat.com/show_bug.cgi?id=56789')
            head_pipeline = {'id': 22334455,
                             'web_url': 'https://gitlab.com/redhat/rhel-8/-/pipelines/22334455'}
            mock_mr = mock.Mock(iid=10, description=description, title="A Cool MR",
                                web_url="https://gitlab.com/project/-/merge_requests/10",
                                work_in_progress=False, labels=['readyForQA'],
                                head_pipeline=head_pipeline)
            mock_project.mergerequests.get.return_value = mock_mr
            bridge = mock.Mock()
            bridge.name = defs.KERNEL_PIPELINES[0]
            web_url = 'https://gitlab.com/group/project/-/pipelines/4321'
            bridge.downstream_pipeline = {'id': 4321, 'web_url': web_url}
            pipeline = mock.Mock(id=22334455, status='success')
            pipeline.bridges.list.return_value = [bridge]
            mock_project.pipelines.get.return_value = pipeline
            mock_ds_project = mock.Mock()
            mock_ds_project.name = 'Cool Downstream Project'
            mock_gl.projects.get.return_value = True
            mock_gl.projects.get.side_effect = [mock_project, mock_ds_project]
            mock_ds_pipeline = mock.Mock(id=4321, status='running')
            mock_ds_project.pipelines.get.return_value = mock_ds_pipeline
            job1 = mock.Mock(stage='build', id=11)
            job2 = mock.Mock(stage='merge', id=12)
            mock_ds_pipeline.jobs.list.return_value = [job1, job2]
            msg = mock.Mock(payload=copy.deepcopy(self.PAYLOAD_PIPELINE))

            buglinker.process_pipeline(msg)
            self.assertIn('Pipeline 4321 is not finished: running', logs.output[-1])
            mock_project.pipelines.get.assert_called_with(22334455)
            pipeline.bridges.list.assert_called()
            mock_ds_project.pipelines.get.assert_called_with(4321)
            mock_post.assert_not_called()

        # No publish jobs.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            description = ('Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=123456\n'
                           'Depends: https://bugzilla.redhat.com/show_bug.cgi?id=56789')
            head_pipeline = {'id': 22334455, 'status': 'success',
                             'web_url': 'https://gitlab.com/redhat/rhel-8/-/pipelines/22334455'}
            mock_mr = mock.Mock(iid=10, description=description, title="A Cool MR",
                                web_url="https://gitlab.com/project/-/merge_requests/10",
                                work_in_progress=False, labels=['readyForQA'],
                                head_pipeline=head_pipeline)
            mock_project.mergerequests.get.return_value = mock_mr
            bridge = mock.Mock()
            bridge.name = defs.KERNEL_PIPELINES[0]
            web_url = 'https://gitlab.com/group/project/-/pipelines/4321'
            bridge.downstream_pipeline = {'id': 4321, 'web_url': web_url}
            pipeline = mock.Mock(id=22334455, status='success')
            pipeline.bridges.list.return_value = [bridge]
            mock_project.pipelines.get.return_value = pipeline
            mock_ds_project = mock.Mock()
            mock_ds_project.name = 'Cool Downstream Project'
            mock_gl.projects.get.return_value = True
            mock_gl.projects.get.side_effect = [mock_project, mock_ds_project]
            mock_ds_pipeline = mock.Mock(id=4321, status='success')
            mock_ds_project.pipelines.get.return_value = mock_ds_pipeline
            mock_ds_pipeline.jobs.list.return_value = [job1, job2]
            msg = mock.Mock(payload=copy.deepcopy(self.PAYLOAD_PIPELINE))

            buglinker.process_pipeline(msg)
            self.assertIn('pipeline 4321 in project Cool Downstream Project', logs.output[-1])
            mock_project.pipelines.get.assert_called_with(22334455)
            pipeline.bridges.list.assert_called()
            mock_ds_project.pipelines.get.assert_called_with(4321)
            mock_post.assert_not_called()

        # No successful publish jobs
        with self.assertLogs('cki.webhook.buglinker', level='DEBUG') as logs:
            job3 = mock.Mock(stage='publish', id=13)
            job4 = mock.Mock(stage='publish', id=14)
            mock_ds_pipeline.jobs.list.return_value = [job1, job2, job3, job4]
            mock_gl.projects.get.side_effect = [mock_project, mock_ds_project]

            buglinker.process_pipeline(msg)
            self.assertIn('No job data.', logs.output[-1])
            mock_post.assert_not_called()

        # No job data.
        with self.assertLogs('cki.webhook.buglinker', level='DEBUG') as logs:
            job3 = mock.Mock(stage='publish', id=13, status='success')
            job3.name = 'publish x86_64'
            job4 = mock.Mock(stage='publish', id=14, status='success')
            job4.name = 'publish s390x'
            job5 = mock.Mock(stage='test', id=15, status='running')
            job5.name = 'test x86_64'
            job6 = mock.Mock(stage='test', id=16, status='success')
            job6.name = 'test s390x'
            job7 = mock.Mock(stage='test', id=17, status='canceled')
            job7.name = 'test ppc64le'
            mock_ds_pipeline.jobs.list.return_value = [job1, job2, job3, job4, job5, job6, job7]
            test_job5 = mock.Mock(id=15, status='running', web_url='https://blablabla15',
                                  stage='test')
            test_job5.name = 'test x86_64'
            test_job6 = mock.Mock(id=16, status='success', web_url='https://blablabla16',
                                  stage='test')
            test_job6.name = 'test s390x'
            test_job7 = mock.Mock(id=17, status='canceled', web_url='https://blablabla17',
                                  stage='test')
            test_job7.name = 'test ppc64le'
            test_job5.artifact.return_value = None
            test_job6.artifact.return_value = None
            test_job7.artifact.return_value = None
            mock_ds_project.jobs.get.side_effect = [test_job5, test_job6, test_job7]
            mock_gl.projects.get.side_effect = [mock_project, mock_ds_project]

            buglinker.process_pipeline(msg)
            self.assertIn('No job data.', logs.output[-1])
            mock_post.assert_not_called()

        test_job5_rc = ("[state]\n"
                        "repo_path = artifacts/repo/4.18.0-123.el8/\n"
                        "kernel_package_url = http://internal/repo/x86_64/4.18.0-123.el8.x86_64\n"
                        "kernel_browse_url = http://gitlab.com/browse/4.18.0-123.el8/\n"
                        "kernel_arch = x86_64\n"
                        "kernel_version = 4.18.0-123.el8\n").encode()

        test_job6_rc = ("[state]\n"
                        "repo_path = artifacts/repo/4.18.0-123.el8/\n"
                        "kernel_package_url = http://internal/repo/x86_64/4.18.0-123.el8.s390x\n"
                        "kernel_browse_url = http://gitlab.com/browse/4.18.0-123.el8/\n"
                        "kernel_arch = s390x\n"
                        "kernel_version = 4.18.0-123.el8\n").encode()

        test_job5.artifact.return_value = test_job5_rc
        test_job6.artifact.return_value = test_job6_rc

        # No bzcon.
        with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
            mock_bz.return_value = False
            mock_ds_project.jobs.get.side_effect = [test_job5, test_job6, test_job7]
            mock_gl.projects.get.side_effect = [mock_project, mock_ds_project]

            buglinker.process_pipeline(msg)
            self.assertIn('No bugzilla connection.', logs.output[-1])
            mock_post.assert_not_called()

        # All good for regular pipeline.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_bz.return_value = True
            mock_ds_project.jobs.get.side_effect = [test_job5, test_job6, test_job7]
            mock_gl.projects.get.side_effect = [mock_project, mock_ds_project]

            buglinker.process_pipeline(msg)
            self.assertIn("Generated comment text:", logs.output[-1])
            mock_post.assert_called_once()
            mock_add_mr.assert_not_called()

        # All good for regular pipeline.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_bz.return_value = True
            mock_ds_project.jobs.get.side_effect = [test_job5, test_job6, test_job7]
            mock_gl.projects.get.side_effect = [mock_project, mock_ds_project]
            mock_post.reset_mock()
            bridge.name = defs.KERNEL_RT_PIPELINES[0]

            buglinker.process_pipeline(msg)
            self.assertIn("Generated comment text:", logs.output[-1])
            mock_post.assert_called_once()
            mock_add_mr.assert_called_once()

    @mock.patch('webhook.buglinker.update_bugzilla')
    @mock.patch('webhook.common.draft_status')
    @mock.patch('webhook.common.try_bugzilla_conn')
    @mock.patch('webhook.buglinker.process_pipeline')
    def test_process_mr(self, mock_process_pipe, mock_trybz, mock_draftcheck, mock_update_bz):
        path_with_namespace = self.PAYLOAD_MERGE['project']['path_with_namespace']
        merge_msg = copy.deepcopy(self.PAYLOAD_MERGE)

        # MR has not changed, ignore event.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_draftcheck.return_value = False, False
            msg = mock.Mock(payload=merge_msg)

            buglinker.process_mr(msg)
            self.assertIn('MR has not changed, ignoring event.', logs.output[-1])
            mock_process_pipe.assert_not_called()
            mock_update_bz.assert_not_called()

        # MR is a Draft, ignore it.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_draftcheck.return_value = True, False
            merge_msg['object_attributes']['action'] = 'open'

            buglinker.process_mr(msg)
            self.assertIn('MR is a Draft, ignoring.', logs.output[-1])
            mock_process_pipe.assert_not_called()
            mock_update_bz.assert_not_called()

        # MR description doesn't list any bugs.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_draftcheck.return_value = False, False

            buglinker.process_mr(msg)
            self.assertIn('No relevant bugs found in MR description.', logs.output[-1])
            mock_process_pipe.assert_not_called()
            mock_update_bz.assert_not_called()

        # No bugzilla connection.
        mr_description = 'Bugzilla: https://bugzilla.redhat.com/12345678'
        merge_msg['object_attributes']['description'] = mr_description
        mock_trybz.return_value = False

        buglinker.process_mr(msg)
        mock_trybz.assert_called_once()
        mock_process_pipe.assert_not_called()
        mock_update_bz.assert_not_called()

        # One bug to link.
        mock_bz = mock.Mock()
        mock_trybz.return_value = mock_bz
        merge_msg['changes'] = {'description': {'previous': 'Bad description',
                                                'current': mr_description}}
        bugs = {'link': {'12345678'},
                'unlink': set()
                }

        buglinker.process_mr(msg)
        mock_update_bz.assert_called_with(bugs, path_with_namespace, 2, mock_bz)

        # Just removed Draft (send to pipeline processor) and one bug to link.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_draftcheck.return_value = False, True

            buglinker.process_mr(msg)
            self.assertIn('Sending MR event to pipeline processor.', logs.output[-1])
            mock_update_bz.assert_called_with(bugs, path_with_namespace, 2, mock_bz)

        # MR changed to Draft state, unlink its bug.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_draftcheck.return_value = True, True
            bugs = {'link': set(),
                    'unlink': {'12345678'}
                    }

            buglinker.process_mr(msg)
            self.assertIn('MR changed to Draft, unlinking bugs.', logs.output[-1])
            mock_update_bz.assert_called_with(bugs, path_with_namespace, 2, mock_bz)
