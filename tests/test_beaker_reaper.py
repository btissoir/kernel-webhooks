"""Test the beaker reaper webhook."""
# pylint: disable=no-self-use
import json
import os
import subprocess
import tempfile
from unittest import TestCase
from unittest import mock

import responses

from webhook import beaker_reaper


@mock.patch.dict(os.environ, {'IS_PRODUCTION': 'false', 'IRC_BOT_EXCHANGE': ''})
class TestBeakerReaper(TestCase):
    """ Test the beaker reaper class."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._mocked_runs = []
        self._mocked_calls = []

    def _mock_run(self, args, *, check=False, **_):
        for run in self._mocked_runs:
            if run[0] == args:
                (returncode, stdout) = run[1:]
                break
        else:
            self.fail(f'Command {args} not found in mocked subprocess.run')
        self._mocked_calls.append(args)
        if check and returncode:
            raise subprocess.CalledProcessError(returncode, args, output=stdout)
        return subprocess.CompletedProcess(args, returncode, stdout=stdout)

    def _add_run_result(self, args, returncode, stdout=None):
        self._mocked_runs.append((args, returncode, stdout))

    def _main_message(self, message):
        with tempfile.NamedTemporaryFile('w') as message_file:
            message_file.write(json.dumps(message))
            message_file.flush()
            beaker_reaper.main(['--json-message-file', message_file.name])

    @mock.patch('webhook.beaker_reaper.Reaper.load_running_jobs')
    @mock.patch('webhook.beaker_reaper.Reaper.process_pipeline_project')
    def test_process_pipeline_project_main(self, process_pipeline_project, load_running_jobs):
        """Check that the CLI correctly proceeds to evaluate projects."""
        process_pipeline_project.return_value = 0
        beaker_reaper.main(['--pipeline-project-url', 'https://project1',
                            '--pipeline-project-url', 'https://project2'])
        load_running_jobs.assert_called_once()
        process_pipeline_project.assert_has_calls([mock.call('https://project1'),
                                                   mock.call('https://project2')])

    @responses.activate
    def test_process_pipeline_project_no_whiteboard(self):
        """Check that a job without a whiteboard is correctly ignored."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={})
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(['bkr', 'job-results', 'J:1'], 0,
                             '<job/>')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            beaker_reaper.main(['--pipeline-project-url', 'https://h/project1'])

    @responses.activate
    def test_process_pipeline_project_wrong_whiteboard(self):
        """Check that a job with a non-matching whiteboard is correctly ignored."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={})
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(['bkr', 'job-results', 'J:1'], 0,
                             '<job><whiteboard>foobar</whiteboard></job>')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            beaker_reaper.main(['--pipeline-project-url', 'https://h/project1'])

    @responses.activate
    def test_process_pipeline_project_missing_pipeline(self):
        """Check that a job with a missing pipeline is correctly ignored."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={'id': 3})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2', status=404)
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(['bkr', 'job-results', 'J:1'], 0,
                             '<job><whiteboard>cki@gitlab:2</whiteboard></job>')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            beaker_reaper.main(['--pipeline-project-url', 'https://h/project1'])

    @responses.activate
    def test_process_pipeline_project_pipeline_running(self):
        """Check that a job with a running pipeline is correctly ignored."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={'id': 3})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2',
                      json={'status': 'running'})
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(['bkr', 'job-results', 'J:1'], 0,
                             '<job><whiteboard>cki@gitlab:2</whiteboard></job>')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            beaker_reaper.main(['--pipeline-project-url', 'https://h/project1'])

    @responses.activate
    @mock.patch.dict(os.environ, {'BEAKER_OWNER': 'owner'})
    def test_process_pipeline_project_owner_filter(self):
        """Check that a job with a running pipeline is correctly ignored."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={'id': 3})
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab', '--unfinished',
                              '--owner', 'owner'], 0, '[]')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            beaker_reaper.main(['--pipeline-project-url', 'https://h/project1'])

    @responses.activate
    @mock.patch.dict(os.environ, {'IS_PRODUCTION': 'true'})
    def test_process_pipeline_project_pipeline_success_cancel_fail(self):
        """Check that errors during cancelling do not affect other jobs."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={'id': 3})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab', '--unfinished'], 0,
                             '["J:1", "J:2"]')
        self._add_run_result(['bkr', 'job-results', 'J:1'], 0,
                             '<job><whiteboard>cki@gitlab:2</whiteboard></job>')
        self._add_run_result(['bkr', 'job-results', 'J:2'], 0,
                             '<job><whiteboard>cki@gitlab:2</whiteboard></job>')
        self._add_run_result(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:1'], 1)
        self._add_run_result(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:2'], 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            self.assertRaises(SystemExit, beaker_reaper.main,
                              ['--pipeline-project-url', 'https://h/project1'])
        self.assertIn(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:1'], self._mocked_calls)
        self.assertIn(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:2'], self._mocked_calls)

    @responses.activate
    @mock.patch.dict(os.environ, {'IS_PRODUCTION': 'true'})
    def test_process_pipeline_project_pipeline_success(self):
        """Check that jobs can be cancelled."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={'id': 3})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(['bkr', 'job-results', 'J:1'], 0,
                             '<job><whiteboard>cki@gitlab:2</whiteboard></job>')
        self._add_run_result(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:1'], 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            beaker_reaper.main(['--pipeline-project-url', 'https://h/project1'])
        self.assertIn(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:1'], self._mocked_calls)

    @mock.patch('webhook.beaker_reaper.Reaper.run')
    def test_run_main(self, run):
        """Check that the message loop is run."""
        beaker_reaper.main([])
        run.assert_called_once()

    @responses.activate
    def test_run_main_running(self):
        """Check that jobs for running pipelines are ignored."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2',
                      json={'status': 'running'})
        self._main_message({'object_kind': 'pipeline',
                            'object_attributes': {'id': 2},
                            'project': {'id': 3, 'web_url': 'https://h/project1'},
                            'user': {'username': 'somebody'},
                            })

    @responses.activate
    def test_run_main_success(self):
        """Check that jobs for finished pipelines are processed."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2/bridges', json=[])
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab:2', '--unfinished'], 0,
                             '["J:1"]')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            self._main_message({'object_kind': 'pipeline',
                                'object_attributes': {'id': 2},
                                'project': {'id': 3, 'web_url': 'https://h/project1'},
                                'user': {'username': 'somebody'},
                                })

    @responses.activate
    @mock.patch.dict(os.environ, {'IS_PRODUCTION': 'true'})
    def test_run_main_success_cancel_exception(self):
        """Check that errors during job cancelling cause exceptions."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2/bridges', json=[])
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab:2', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:1'], 1)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            self.assertRaises(subprocess.CalledProcessError, self._main_message, {
                'object_kind': 'pipeline',
                'object_attributes': {'id': 2},
                'project': {'id': 3, 'web_url': 'https://h/project1'},
                'user': {'username': 'somebody'},
            })

    @responses.activate
    @mock.patch.dict(os.environ, {'IS_PRODUCTION': 'true',
                                  'BEAKER_URL': 'beaker_url',
                                  'IRC_BOT_EXCHANGE': 'exchange'})
    @mock.patch('cki_lib.messagequeue.MessageQueue.send_message')
    def test_run_main_success_cancel(self, send_message):
        """Check that jobs can be cancelled."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2/bridges', json=[])
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab:2', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:1'], 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            self._main_message({
                'object_kind': 'pipeline',
                'object_attributes': {'id': 2},
                'project': {'id': 3, 'web_url': 'https://h/project1'},
                'user': {'username': 'somebody'},
            })
        send_message.assert_called()

    @responses.activate
    @mock.patch.dict(os.environ, {'IS_PRODUCTION': 'true',
                                  'IRC_BOT_EXCHANGE': 'exchange'})
    @mock.patch('cki_lib.messagequeue.MessageQueue.send_message')
    def test_run_main_parent_parent_running(self, send_message):
        """Check that jobs for finished pipelines are processed."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2/bridges',
                      json=[{'downstream_pipeline': {
                          'id': 5,
                          'project_id': 4,
                          'status': 'running'}}])
        responses.add(responses.GET, 'https://h/api/v4/projects/4/pipelines/5',
                      json={'id': 5, 'status': 'running', 'web_url': 'child_url'})
        responses.add(responses.POST, 'https://h/api/v4/projects/4/pipelines/5/cancel',
                      json={})
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            self._main_message({'object_kind': 'pipeline',
                                'object_attributes': {'id': 2},
                                'project': {'id': 3, 'web_url': 'https://h/project1'},
                                'user': {'username': 'somebody'},
                                })
        self.assertEqual(responses.calls[-1].request.method, responses.POST)
        send_message.assert_called()

    @responses.activate
    @mock.patch.dict(os.environ, {'IS_PRODUCTION': 'true'})
    def test_run_main_parent_success(self):
        """Check that jobs for finished pipelines are processed."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2/bridges',
                      json=[{'downstream_pipeline': {
                          'id': 5,
                          'project_id': 4,
                          'status': 'success'}}])
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            self._main_message({'object_kind': 'pipeline',
                                'object_attributes': {'id': 2},
                                'project': {'id': 3, 'web_url': 'https://h/project1'},
                                'user': {'username': 'somebody'},
                                })

    @responses.activate
    def test_run_main_parent_running_report(self):
        """Check that jobs for finished pipelines are processed."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2/bridges',
                      json=[{'downstream_pipeline': {
                          'id': 5,
                          'project_id': 4,
                          'status': 'running'}}])
        responses.add(responses.GET, 'https://h/api/v4/projects/4/pipelines/5',
                      json={'id': 5, 'status': 'running', 'web_url': 'child_url'})
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            self._main_message({'object_kind': 'pipeline',
                                'object_attributes': {'id': 2},
                                'project': {'id': 3, 'web_url': 'https://h/project1'},
                                'user': {'username': 'somebody'},
                                })
