"""Webhook interaction tests."""
import copy
from unittest import TestCase
from unittest import mock

from webhook import ack_nack
from webhook import common
from webhook import defs
from webhook import owners


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestAckNack(TestCase):
    PAYLOAD_NOTE = {'object_kind': 'note',
                    'user': {'id': 1, 'username': 'user1'},
                    'project': {'id': 1,
                                'web_url': 'https://web.url/g/p'},
                    'object_attributes': {'iid': 1,
                                          'noteable_type': 'MergeRequest',
                                          'note': 'comment',
                                          'last_commit': {'timestamp': '2021-01-08T20:00:00.000Z'}},
                    'merge_request': {'target_branch': 'main', 'iid': 2},
                    'state': 'opened'
                    }

    @mock.patch('webhook.ack_nack._edit_approval_rule')
    def test_get_reviewers(self, ear):
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   labels:\n"
                       "     name: SomeSubsystem\n"
                       "   required-approval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "     - name: User 3\n"
                       "       email: user3@redhat.com\n"
                       "       restricted: true\n"
                       "   reviewers:\n"
                       "     - name: User 4\n"
                       "       email: user4@redhat.com\n"
                       "       restricted: false\n"
                       "     - name: User 5\n"
                       "       email: user5@redhat.com\n"
                       "     - name: User 6\n"
                       "       email: user6@redhat.com\n"
                       "       restricted: true\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n"
                       " - subsystem: Another Subsystem\n"
                       "   labels:\n"
                       "     name: AnotherSubsystem\n"
                       "   required-approval: false\n"
                       "   maintainers:\n"
                       "     - name: User 7\n"
                       "       email: user7@redhat.com\n"
                       "   reviewers:\n"
                       "     - name: User 8\n"
                       "       email: user8@redhat.com\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - Documentation/\n")
        owners_parser = owners.Parser(owners_yaml)
        gl_instance = mock.Mock()
        gl_mergerequest = mock.Mock()
        reviewers, required = ack_nack._get_reviewers(gl_instance, gl_mergerequest,
                                                      ['redhat/Makefile'], owners_parser,
                                                      'user1@redhat.com')
        # user1@redhat.com is the merge request submitter and shouldn't be in the reviewer list.
        self.assertEqual(reviewers, [(frozenset(['user2@redhat.com', 'user3@redhat.com',
                                                 'user4@redhat.com', 'user5@redhat.com',
                                                 'user6@redhat.com']), 'SomeSubsystem')])
        self.assertEqual(required, [(frozenset(['user2@redhat.com', 'user4@redhat.com',
                                                'user5@redhat.com']), 'SomeSubsystem')])

        reviewers, required = ack_nack._get_reviewers(gl_instance, gl_mergerequest,
                                                      [], owners_parser,
                                                      'user1@redhat.com')
        self.assertEqual(reviewers, [])
        self.assertEqual(required, [])

        reviewers, required = ack_nack._get_reviewers(gl_instance, gl_mergerequest,
                                                      ['Documentation/foo'],
                                                      owners_parser, 'user1@redhat.com')
        self.assertEqual(reviewers, [(frozenset(['user7@redhat.com', 'user8@redhat.com']),
                                      'AnotherSubsystem')])
        self.assertEqual(required, [])

    def test_get_reviewers_no_self_review(self):
        # The person that opened the merge request is the only person listed as a subsystem
        # maintainer. That person should not be listed as a required reviewer.
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   required-approval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n")
        owners_parser = owners.Parser(owners_yaml)
        gl_instance = mock.Mock()
        gl_mergerequest = mock.Mock()
        notified, reviewers = ack_nack._get_reviewers(gl_instance, gl_mergerequest,
                                                      ['redhat/Makefile'], owners_parser,
                                                      'user1@redhat.com')
        self.assertEqual(notified, [])
        self.assertEqual(reviewers, [])

    def test_ensure_base_approval_rule(self):
        gl_project = mock.Mock()
        gl_project.name = "Testing"
        gl_mergerequest = mock.Mock()
        gl_mergerequest.state = "opened"
        gl_mergerequest.draft = False
        gl_mergerequest.iid = 666
        all_members_rule = mock.Mock()
        all_members_rule.approvals_required = 2
        all_members_rule.id = 1234
        gl_project.approvalrules.list.return_value = [all_members_rule]
        mr_all_members_rule = mock.Mock()
        mr_all_members_rule.name = "All Members"
        mr_all_members_rule.approvals_required = 2
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        # Everything is fine
        with self.assertLogs('cki.webhook.ack_nack', level='DEBUG') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('DEBUG:cki.webhook.ack_nack:Testing MR 666 has '
                              '\'All Members\' rule set appropriately'))
        # Project has no All Members rule
        gl_project.approvalrules.list.return_value = []
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Project Testing has no '
                              '\'All Members\' approval rule'))
        # Project has required approvals set to 0
        all_members_rule.approvals_required = 0
        gl_project.approvalrules.list.return_value = [all_members_rule]
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Project Testing isn\'t '
                              'requiring any reviewers for MRs'))
        # MR had approvals required set to 0
        all_members_rule.approvals_required = 2
        gl_project.approvalrules.list.return_value = [all_members_rule]
        mr_all_members_rule.approvals_required = 0
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Testing MR 666 had '
                              'approvals required set to 0'))
        # MR had no base approvals rule present
        gl_mergerequest.approval_rules.list.return_value = []
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Testing MR 666 had '
                              'no base approvals required'))

    @mock.patch('cki_lib.misc.is_production', return_value=True)
    @mock.patch('webhook.ack_nack._emails_to_gl_user_ids')
    def test_edit_approval_rule(self, etgluid, is_prod):
        gl_instance = mock.Mock()
        gl_mergerequest = mock.Mock()
        etgluid.return_value = [105, 106]
        rule1 = mock.Mock()
        rule1.name = "yeehaw"
        gl_mergerequest.approval_rules.list.return_value = [rule1]

        subsystem = "george"
        reviewers = ["user1@redhat.com", "user2@redhat.com"]
        num_req = 0
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._edit_approval_rule(gl_instance, gl_mergerequest,
                                         subsystem, reviewers, num_req)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:Create rule for ss george, '
                              'with 0 required approval(s) from user(s) '
                              '[\'user1@redhat.com\', \'user2@redhat.com\']'))

        subsystem = "bob"
        reviewers = ["user1@redhat.com", "user2@redhat.com"]
        num_req = 2
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._edit_approval_rule(gl_instance, gl_mergerequest,
                                         subsystem, reviewers, num_req)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:Create rule for ss bob, '
                              'with 2 required approval(s) from user(s) '
                              '[\'user1@redhat.com\', \'user2@redhat.com\']'))

        subsystem = "yeehaw"
        with self.assertLogs('cki.webhook.ack_nack', level='DEBUG') as logs:
            ack_nack._edit_approval_rule(gl_instance, gl_mergerequest,
                                         subsystem, reviewers, num_req)
            self.assertEqual(logs.output[-1],
                             ("INFO:cki.webhook.ack_nack:Approval rule for subsystem yeehaw "
                              "already exists"))

    def test_get_ark_config_mr_ccs(self):
        mr = mock.Mock()
        mr.description = ''
        self.assertEqual(ack_nack.get_ark_config_mr_ccs(mr), [])
        mr.description = 'Cool description.\nCc: user1 <user1@redhat.com>\n'
        self.assertEqual(ack_nack.get_ark_config_mr_ccs(mr), ['user1@redhat.com'])

    @mock.patch('webhook.ack_nack.get_ark_config_mr_ccs')
    def test_get_ark_reviewers(self, mocked_getarkconfig):
        mocked_mr = mock.Mock()
        ack_nack.get_ark_config_mr_ccs.return_value = []
        cc_reviewers = \
            ack_nack.get_ark_reviewers(defs.ARK_PROJECT_ID, mocked_mr,
                                       ['redhat/configs/hi', 'net/core/dev.c'])
        self.assertFalse(cc_reviewers)
        ack_nack.get_ark_config_mr_ccs.return_value = ['ark_user1@redhat.com']
        cc_reviewers = \
            ack_nack.get_ark_reviewers(defs.ARK_PROJECT_ID, mocked_mr,
                                       ['redhat/configs/hi'])
        self.assertEqual(cc_reviewers, [({'ark_user1@redhat.com'}, None)])

    def test_parse_tag(self):
        tag = ack_nack._parse_tag('Acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Acked-by', 'User 1', 'user1@redhat.com'))

        tag = ack_nack._parse_tag('LGTM!\n\nAcked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Acked-by', 'User 1', 'user1@redhat.com'))

        tag = ack_nack._parse_tag(r'Acked-by: User 1 \<user1@redhat.com\>')
        self.assertEqual(tag, ('Acked-by', 'User 1', 'user1@redhat.com'))

        tag = ack_nack._parse_tag('Rescind-acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Rescind-acked-by', 'User 1', 'user1@redhat.com'))

        tag = ack_nack._parse_tag('Revoke-acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Revoke-acked-by', 'User 1', 'user1@redhat.com'))

        tag = ack_nack._parse_tag('Nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Nacked-by', 'User 2', 'user2@redhat.com'))

        tag = ack_nack._parse_tag('Rescind-nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Rescind-nacked-by', 'User 2', 'user2@redhat.com'))

        tag = ack_nack._parse_tag('Revoke-nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Revoke-nacked-by', 'User 2', 'user2@redhat.com'))

        tag = ack_nack._parse_tag('Unknown-tag: User 3 <user3@redhat.com>')
        self.assertIsNone(tag[0])

        tag = ack_nack._parse_tag('You forgot your Acked-by: User 1 <user1@redhat.com>')
        self.assertIsNone(tag[0])

        tag = ack_nack._parse_tag('Acked-by: Invalid Format')
        self.assertIsNone(tag[0])

        tag = ack_nack._parse_tag(('Acked-by: User 1 <user1@redhat.com>\n'
                                   '\n'
                                   '> patch title\n'
                                   '> Signed-off-by: User 2 <user2@redhat.com>\n'))
        self.assertEqual(tag, ('Acked-by', 'User 1', 'user1@redhat.com'))

    @mock.patch('webhook.cdlib.mr_code_changed', mock.Mock(return_value=(True, False)))
    @mock.patch('webhook.cdlib.get_last_code_changed_timestamp')
    def test_process_acks_nacks(self, ccts):
        ccts.return_value = None
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user2@redhat.com',
                     'user2'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        (acks, nacks, _, _) = ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                           'user3@redhat.com', 'mocked',
                                                           'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, {('User 1', 'user1@redhat.com'), ('User 2', 'user2@redhat.com')})
        self.assertEqual(nacks, set([]))

        ccts.return_value = '2021-05-01T19:40:00.000Z'
        (acks, nacks, _, _) = ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                           'user3@redhat.com', 'mocked',
                                                           'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, {('User 1', 'user1@redhat.com'), ('User 2', 'user2@redhat.com')})
        self.assertEqual(nacks, set([]))

        ccts.return_value = None
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user2@redhat.com',
                     'user2'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Nacked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        msgs.append(('2021-01-09T19:50:00.000Z', 'Rescind-acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        (acks, nacks, _, _) = ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                           'user3@redhat.com', 'mocked',
                                                           'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, {('User 2', 'user2@redhat.com')})

        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user2@edhat.com',
                     'user2'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Nacked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        msgs.append(('2021-01-09T19:50:00.000Z', 'Another comment', 'user2@redhat.com', 'user2'))
        msgs.append(('2021-01-09T19:52:00.000Z', 'Revoke-nacked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        (acks, nacks, _, _) = ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                           'user3@redhat.com', 'mocked',
                                                           'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, {('User 1', 'user1@redhat.com')})
        self.assertEqual(nacks, set([]))

        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Nacked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user1@redhat.com',
                     'user1'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        (acks, nacks, _, _) = ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                           'user3@redhat.com', 'mocked',
                                                           'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, {('User 1', 'user1@redhat.com')})
        self.assertEqual(nacks, set([]))

        # Don't allow self ACKs
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user3@redhat.com', 'user3'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 3 <user3@redhat.com>',
                     'user3@redhat.com', 'user3'))
        (acks, nacks, _, _) = ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                           'user3@redhat.com', 'mocked', 'mocked',
                                                           'mocked', 'mocked')
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, set([]))

        # Primary email not set on account
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user3@redhat.com', 'user3'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 3 <user3@redhat.com>',
                     'user3@redhat.com', 'user3'))
        (acks, nacks, _, _) = ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z', None,
                                                           'mocked', 'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, {('User 3', 'user3@redhat.com')})
        self.assertEqual(nacks, set([]))

        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        # This Ack should not be counted since it's before the last_commit_timestamp
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-14T07:00:00.000Z', 'Yet another comment', 'user2@redhat.com',
                     'user2'))
        msgs.append(('2021-01-14T07:00:00.000Z', 'Acked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        (acks, nacks, _, _) = ack_nack._process_acks_nacks(msgs, '2021-01-10T20:00:00.000Z',
                                                           'user3@redhat.com', 'mocked',
                                                           'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, {('User 2', 'user2@redhat.com')})
        self.assertEqual(nacks, set([]))

        # Do not fail if the payload object_attributes does not have a last_commit attribute.
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Nacked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user1@redhat.com',
                     'user1'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        (acks, nacks, _, _) = ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com', 'mocked',
                                                           'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, {('User 1', 'user1@redhat.com')})
        self.assertEqual(nacks, set([]))

        # Ignore ACK with forged address
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'someuser@gmail.com', 'someuser'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'someuser@gmail.com', 'someuser'))
        (acks, nacks, _, _) = ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com', 'mocked',
                                                           'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, set([]))

        # Public from address not set
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'someuser@gmail.com', 'someuser'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Acked-by: User 1 <user1@redhat.com>', None,
                     'user1'))
        (acks, nacks, _, _) = ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com', 'mocked',
                                                           'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, set([]))

        # cki-bot can impersonate users
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z',
                     'Some User user1@redhat.com commented via email:\n'
                     'Acked-by: User 1 <user1@redhat.com>', None, 'cki-bot'))
        (acks, nacks, _, _) = ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com', 'mocked',
                                                           'mocked', 'mocked', 'mocked')
        self.assertEqual(acks, {('User 1', 'user1@redhat.com')})
        self.assertEqual(nacks, set([]))

        # make sure email bridge acks aren't forged either
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z',
                     'Some User <baduser@redhat.com> commented via email:\n'
                     'Acked-by: User 1 <user1@redhat.com>', None,
                     defs.EMAIL_BRIDGE_ACCOUNT))
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            (acks, nacks, _, _) = ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com',
                                                               'mocked', 'mocked', 'mocked',
                                                               'mocked')
            self.assertEqual(acks, set([]))
            self.assertEqual(nacks, set([]))

            self.assertIn(('Rejecting apparent forged ack/nack attempt (baduser@redhat.com '
                           '!= user1@redhat.com)'),
                          logs.output[-1])

    def test_get_reviewers_from_approval_rules(self):
        R1EA = [{'id': 12345, 'name': 'Ram Eater', 'username': 'ram'},
                {'id': 56789, 'name': 'Mike Memory', 'username': 'mmem'},
                {'id': 8675309, 'name': 'Jenny Cache', 'username': 'jcache'}]
        R2EA = [{'id': 1234, 'name': 'Joe Developer', 'username': 'jdev'},
                {'id': 5678, 'name': 'Bob Reviewer', 'username': 'bob'}]
        APPROVED_BY = [{'user': {'id': 12345, 'name': 'Ram Eater', 'username': 'ram'}},
                       {'user': {'id': 5678, 'name': 'Bob Reviewer', 'username': 'bob'}}]
        MM_RESULT = ['mm', 1, 1, ['ram', 'mmem', 'jcache'], [12345, 56789, 8675309]]
        X86_RESULT = ['x86', 0, 1, ['jdev', 'bob'], [1234, 5678]]

        rule1 = mock.Mock()
        rule1.name = 'mm'
        rule1.eligible_approvers = R1EA
        rule1.approvals_required = 1

        rule2 = mock.Mock()
        rule2.name = 'x86'
        rule2.eligible_approvers = R2EA
        rule2.approvals_required = 0

        approvals = mock.Mock()
        approvals.approved = False
        approvals.approvals_required = 3
        approvals.approvals_left = 1
        approvals.approved_by = APPROVED_BY

        gl_mergerequest = mock.Mock()
        gl_mergerequest.approvals.get.return_value = approvals
        gl_mergerequest.approval_rules.list.return_value = [rule1, rule2]

        result = ack_nack.get_reviewers_from_approval_rules(gl_mergerequest)
        self.assertEqual(result, [MM_RESULT, X86_RESULT])

    @mock.patch('webhook.ack_nack._lookup_gitlab_email')
    @mock.patch('webhook.ack_nack._get_old_subsystems_from_labels')
    def test_get_ack_nack_summary(self, old_subsystems, gl_email):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_mergerequest = mock.Mock()

        gl_project.only_allow_merge_if_all_discussions_are_resolved = True
        gl_mergerequest.changes.return_value = {'blocking_discussions_resolved': True}
        gl_mergerequest.resourcelabelevents.list.return_value = []
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=2)
        old_subsystems.return_value = ([], [])
        self.assertEqual(ack_nack._get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [], [], [], [], []),
                         ('NeedsReview', [], 'Requires 2 more ACK(s).'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1)
        self.assertEqual(ack_nack._get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [('User 1', 'user1@redhat.com')],
                                                        [], [], [], []),
                         ('NeedsReview', [],
                          'ACKed by User 1 <user1@redhat.com>.\n Requires 1 more ACK(s).'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0)
        self.assertEqual(ack_nack._get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [('User 1', 'user1@redhat.com'),
                                                         ('User 2', 'user2@redhat.com')],
                                                        [], [], [], []),
                         ('OK', [],
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>.\n '
                          'Merge Request has all necessary Approvals.'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0)
        gl_mergerequest.changes.return_value = {'blocking_discussions_resolved': False}
        self.assertEqual(ack_nack._get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [('User 1', 'user1@redhat.com'),
                                                         ('User 2', 'user2@redhat.com')],
                                                        [], [], [], []),
                         ('Blocked', [], 'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.'
                                         'com>.\n All discussions must be resolved.'))
        gl_mergerequest.changes.return_value = {'blocking_discussions_resolved': True}

        self.assertEqual(ack_nack._get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [('User 1', 'user1@redhat.com'),
                                                         ('User 2', 'user2@redhat.com')], [],
                                                        [(['user1@redhat.com', 'user2@redhat.com'],
                                                          'subsys1')], [], []),
                         ('OK', ['Acks::subsys1::OK'],
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>.\n '
                          'Merge Request has all necessary Approvals.'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0)
        self.assertEqual(ack_nack._get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [('User 1', 'user1@redhat.com'),
                                                         ('User 2', 'user2@redhat.com')], [],
                                                        [(['user1@redhat.com', 'user3@redhat.com'],
                                                          'subsys1')], [], []),
                         ('OK', ['Acks::subsys1::OK'],
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>.\n '
                          'Merge Request has all necessary Approvals.'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0)
        self.assertEqual(ack_nack._get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [('User 1', 'user1@redhat.com'),
                                                         ('User 2', 'user2@redhat.com')],
                                                        [('User 3', 'user3@redhat.com')],
                                                        [(['user1@redhat.com', 'user2@redhat.com'],
                                                          'subsys1')], [], []),
                         ('NACKed', ['Acks::subsys1::OK'],
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>.\n ' +
                          'NACKed by User 3 <user3@redhat.com>.'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1)
        self.assertEqual(ack_nack._get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [('User 1', 'user1@redhat.com'),
                                                         ('User 2', 'user2@redhat.com'),
                                                         ('User 3', 'user3@redhat.com')], [],
                                                        [(['user1@redhat.com', 'user2@redhat.com'],
                                                          'subsys1'),
                                                         (['user4@redhat.com', 'user5@redhat.com'],
                                                          'subsys2')], [], []),
                         ('NeedsReview', ['Acks::subsys1::OK', 'Acks::subsys2::NeedsReview'],
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>, ' +
                          'User 3 <user3@redhat.com>.\n'))

        self.assertEqual(ack_nack._get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [('User 1', 'user1@redhat.com'),
                                                         ('User 4', 'user4@redhat.com')],
                                                        [('User 2', 'user2@redhat.com')],
                                                        [(['user1@redhat.com', 'user2@redhat.com'],
                                                          'subsys1'),
                                                         (['user4@redhat.com', 'user5@redhat.com'],
                                                          'subsys2')], [], []),
                         ('NACKed', ['Acks::subsys1::NACKed', 'Acks::subsys2::OK'],
                          'ACKed by User 1 <user1@redhat.com>, User 4 <user4@redhat.com>.\n ' +
                          'NACKed by User 2 <user2@redhat.com>.'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1)
        self.assertEqual(ack_nack._get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [('User 1', 'user1@redhat.com'),
                                                         ('User 2', 'user2@redhat.com'),
                                                         ('User 3', 'user3@redhat.com')], [],
                                                        [(['user1@redhat.com', 'user2@redhat.com'],
                                                          'subsys1'),
                                                         (['user4@redhat.com', 'user5@redhat.com'],
                                                          'subsys2'),
                                                         (['user6@redhat.com', 'user7@redhat.com'],
                                                          'subsys3'),
                                                         (['user1@redhat.com', 'user8@redhat.com'],
                                                          'subsys4')], [], []),
                         ('NeedsReview', ['Acks::subsys1::OK', 'Acks::subsys2::NeedsReview',
                                          'Acks::subsys3::NeedsReview', 'Acks::subsys4::OK'],
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>, ' +
                          'User 3 <user3@redhat.com>.\n'))

        # External email address
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=3,
                                                               approvals_left=1)
        self.assertEqual(ack_nack._get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [('User 1', 'user1@redhat.com'),
                                                         ('User 2', 'user2@gmail.com')], [],
                                                        [(['user1@redhat.com', 'user2@redhat.com'],
                                                          'subsys1')], [], []),
                         ('NeedsReview', ['Acks::subsys1::OK'],
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@gmail.com>.\n ' +
                          'Requires 1 more ACK(s).'))

        # Approval Rules Reviewers based Required Approvals
        arr1 = ['RuleX', 1, 0, ['joe'], [101]]
        arr2 = ['RuleY', 0, 0, ['bob'], [102]]
        arr3 = ['RuleZ', 1, 1, ['tim'], [103]]
        gl_email.return_value = 'joe@redhat.com'
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1)
        self.assertEqual(ack_nack._get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [], [], [], [arr1], []),
                         ('NeedsReview', [],
                          '\n Required Approvals:  \n'
                          ' - Approval Rule "RuleX" requires at least 1 ACK(s) '
                          '(0 given) from set (joe@redhat.com).  \n'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1)
        gl_email.return_value = 'bob@redhat.com'
        self.assertEqual(ack_nack._get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [('Bob Reviewer', 'bob@redhat.com')],
                                                        [], [], [arr2], []),
                         ('NeedsReview', [],
                          'ACKed by Bob Reviewer <bob@redhat.com>.\n '
                          '\n Optional Approvals:  \n'
                          ' - Approval Rule "RuleY" requests optional ACK(s) '
                          'from set (bob@redhat.com).  \n'
                          ' Requires 1 more ACK(s).'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0)
        gl_email.return_value = 'bob@redhat.com'
        self.assertEqual(ack_nack._get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [('Bob Reviewer', 'bob@redhat.com'),
                                                         ('Joe Developer', 'joedev@redhat.com')],
                                                        [], [], [arr2, arr3], []),
                         ('OK', [],
                          'ACKed by Bob Reviewer <bob@redhat.com>, '
                          'Joe Developer <joedev@redhat.com>.\n '
                          '\n Optional Approvals:  \n'
                          ' - Approval Rule "RuleY" requests optional ACK(s) '
                          'from set (bob@redhat.com).  \n'
                          ' Satisfied Approvals:  \n'
                          ' - Approval Rule "RuleZ" already has 1 ACK(s) (1 required).  \n'
                          ' Merge Request has all necessary Approvals.'))

    @mock.patch('webhook.ack_nack.get_reviewers_from_approval_rules')
    @mock.patch('webhook.cdlib.set_dependencies_label')
    @mock.patch('webhook.cdlib.get_filtered_changed_files')
    @mock.patch('webhook.ack_nack._do_assign_reviewers')
    @mock.patch('webhook.cdlib.mr_code_changed', mock.Mock(return_value=(True, False)))
    def test_process_merge_request1(self, assign_reviewers, get_files, set_label, rfar):
        get_files.return_value = ['include/linux/netdevice.h']
        set_label.return_value = "Dependencies::OK"
        rfar.return_value = []

        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   required-approval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - kernel/\n")
        owners_parser = owners.Parser(owners_yaml)

        msgs = []
        msgs.append(self.__create_note_body('Some comment', '2021-01-09T19:40:00.000Z', 1))
        msgs.append(self.__create_note_body('Another comment', '2021-01-09T19:42:00.000Z', 1))
        msgs.append(self.__create_note_body('Acked-by: User 1 <user1@redhat.com>',
                                            '2021-01-09T19:44:00.000Z', 1))
        msgs.append(self.__create_note_body('Yet another comment', '2021-01-09T19:46:00.000Z', 2))
        msgs.append(self.__create_note_body('Acked-by: User 2 <user2@redhat.com>',
                                            '2021-01-09T19:48:00.000Z', 2))
        msgs.append(self.__create_note_body('Acked-by: Impersonated User <notuser3@redhat.com>',
                                            '2021-01-09T19:48:00.000Z', 3))

        gl_mergerequest = mock.Mock()
        gl_mergerequest.author = {'id': 4, 'username': 'someuser'}
        gl_mergerequest.notes.list.return_value = msgs
        gl_mergerequest.changes.return_value = {'changes': [{'new_path': 'kernel/fork.c'}],
                                                'blocking_discussions_resolved': True}
        gl_mergerequest.labels = ['Dependencies::OK']
        gl_mergerequest.commits.return_value = \
            [mock.Mock(committed_date='2021-01-08T20:00:00.000Z')]
        gl_mergerequest.reviewers = []
        gl_mergerequest.resourcelabelevents.list.return_value = []
        gl_mergerequest.diffs.list.return_value = []
        gl_mergerequest.approval_rules.list.return_value = []
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0)

        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.labels.list.return_value = []
        gl_project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")
        gl_project.commits.get.return_value = mock.Mock(parent_ids=["1234"])

        gl_instance = mock.Mock()
        gl_instance.users.get = \
            lambda author_id: mock.Mock(public_email=f'user{author_id}@redhat.com')
        gl_instance.users.list = \
            lambda search: [mock.Mock(public_email=search, username=search.split('@')[0])]
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]

        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest,
                                           owners_parser, 'mocked', True)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:ACK/NACK Summary: OK - '
                              'ACKed by User 1 <user1@redhat.com>, '
                              'User 2 <user2@redhat.com>.\n '
                              'Merge Request has all necessary Approvals.'))
            assign_reviewers.assert_called_with(gl_mergerequest, {'user1', 'user2'})

        set_label.return_value = 'Dependencies::deadbeefabcd'
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest,
                                           owners_parser, 'mocked', True)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:ACK/NACK Summary: OK - '
                              'ACKed by User 1 <user1@redhat.com>, '
                              'User 2 <user2@redhat.com>.\n '
                              'Merge Request has all necessary Approvals.'))
            get_files.assert_called_once()

        gl_project.id = defs.ARK_PROJECT_ID
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest,
                                           owners_parser, 'mocked', True)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:ACK/NACK Summary: OK - '
                              'ACKed by User 1 <user1@redhat.com>, '
                              'User 2 <user2@redhat.com>.\n '
                              'Merge Request has all necessary Approvals.'))

    @mock.patch('webhook.cdlib.get_mr_pathlist')
    @mock.patch('webhook.common.extract_all_bzs')
    @mock.patch('webhook.common.extract_dependencies')
    @mock.patch('webhook.cdlib.mr_code_changed', mock.Mock(return_value=(True, True)))
    def test_process_merge_request2(self, ext_deps, ext_bzs, get_paths):
        ext_deps.return_value = []
        ext_bzs.return_value = ([], [], [])
        get_paths.return_value = []

        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   required-approval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - kernel/\n")
        owners_parser = owners.Parser(owners_yaml)

        msgs = []
        msgs.append(self.__create_note_body('Some comment', '2021-01-09T19:40:00.000Z', 1))
        msgs.append(self.__create_note_body('Another comment', '2021-01-09T19:42:00.000Z', 1))
        msgs.append(self.__create_note_body('Acked-by: User 1 <user1@redhat.com>',
                                            '2021-01-09T19:44:00.000Z', 1))
        msgs.append(self.__create_note_body('Yet another comment', '2021-01-09T19:46:00.000Z', 2))
        # Code is pushed up based on committed_date below; previous ack is no longer valid
        msgs.append(self.__create_note_body('Acked-by: User 2 <user2@redhat.com>',
                                            '2021-01-10T07:00:00.000Z', 2))

        gl_mergerequest = mock.Mock()
        gl_mergerequest.author = {'id': 4, 'username': 'someuser'}
        gl_mergerequest.notes.list.return_value = msgs
        gl_mergerequest.changes.return_value = {'changes': [{'new_path': 'kernel/fork.c'}]}
        gl_mergerequest.labels = ['Dependencies::OK']
        gl_mergerequest.reviewers = []
        gl_mergerequest.resourcelabelevents.list.return_value = []
        diff = mock.MagicMock(id=1234, created_at='2021-01-10T03:00:00.000Z')
        gl_mergerequest.diffs.list.return_value = [diff]
        gl_mergerequest.approval_rules.list.return_value = []
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1)

        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.labels.list.return_value = []
        gl_project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")

        gl_instance = mock.Mock()
        gl_instance.users.get = \
            lambda author_id: mock.Mock(public_email=f'user{author_id}@redhat.com')
        gl_instance.users.list.return_value = [mock.Mock(public_email='user1@redhat.com',
                                                         username='user1')]
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]

        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest, owners_parser,
                                           'mocked', True)
            self.assertEqual(logs.output[-2],
                             ('INFO:cki.webhook.ack_nack:ACK/NACK Summary: NeedsReview '
                              '- ACKed by User 2 <user2@redhat.com>.\n Requires 1 more ACK(s).'))
            self.assertEqual(logs.output[-1],
                             'INFO:cki.webhook.ack_nack:Reported stale Approvals from: @user1')

    def __create_note_body(self, body, timestamp, author_id):
        ret = mock.Mock()
        ret.body = body
        ret.updated_at = timestamp
        ret.author = {'id': author_id, 'username': f'user{author_id}'}
        return ret

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message0(self, mocked_process_mr, mock_gl):
        self._test_note("request-ack-nack-evaluation", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called_once()

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message1(self, mocked_process_mr, mock_gl):
        self._test_note("request-evaluation", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called_once()

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message2(self, mocked_process_mr, mock_gl):
        self._test_note("Acked-by: User 1 <user1@redhat.com>", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called_once()

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message3(self, mocked_process_mr, mock_gl):
        # Merge request author public email will be set to user2@redhat.com below
        # so this should fail since it's a user trying to self-ack a patch.
        with self.assertLogs('cki.webhook.ack_nack', level='WARNING') as logs:
            self._test_note("Acked-by: User 2 <user2@redhat.com>", mocked_process_mr, mock_gl)
            mocked_process_mr.assert_not_called()
            self.assertIn("user2@redhat.com cannot self-ack merge request", logs.output[-1])

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message4(self, mocked_process_mr, mock_gl):
        # User's public email doesn't match the address in the Acked-by so this should fail.
        with self.assertLogs('cki.webhook.ack_nack', level='WARNING') as logs:
            self._test_note("Acked-by: User 1 <user1@gmail.com>", mocked_process_mr, mock_gl)
            mocked_process_mr.assert_not_called()
            self.assertIn(("Ignoring 'Acked-by <user1@gmail.com>' since this doesn't match "
                           "the user's public email address user1@redhat.com on GitLab"),
                          logs.output[-1])

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message5(self, mocked_process_mr, mock_gl):
        self._test_note("Some comment", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_not_called()

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message6(self, mocked_process_mr, mock_gl):
        # blocking_discussions_resolved is False and MR has Acks::OK
        self._test_note("Some comment", mocked_process_mr, mock_gl, False, ['Acks::OK'])
        mocked_process_mr.assert_called()

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message7(self, mocked_process_mr, mock_gl):
        # blocking_discussions_resolved is True and MR has Acks::Blocked
        self._test_note("Some comment", mocked_process_mr, mock_gl, True, ['Acks::Blocked'])
        mocked_process_mr.assert_called()

    def _test_note(self, note_text, mocked_process_mr, mock_gl, discussions_resolved=True,
                   labels=None):
        resolv = discussions_resolved
        if not labels:
            labels = []
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = mock.Mock(author={'id': 2, 'username': 'user1'},
                                                              blocking_discussions_resolved=resolv,
                                                              labels=labels)
        gl_instance.projects.get.return_value = gl_project
        gl_instance.users.get = lambda user_id: mock.Mock(public_email=f'user{user_id}@redhat.com',
                                                          username=f'user{user_id}')
        mock_gl.return_value.__enter__.return_value = gl_instance

        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = note_text
        headers = {'message-type': 'gitlab'}
        self.assertTrue(common.process_message(ack_nack.WEBHOOKS, "ROUTING_KEY", payload, headers,
                                               owners_parser='mocked', rhkernel_src='mocked'))

    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_mr_webhook_other_label(self, mock_process_mr):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_instance.projects.get.return_value = gl_project
        gl_mergerequest = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        msg = mock.Mock()
        msg.payload = self._create_merge_payload('update')

        # Change a label not related to ack/nack
        msg.payload['changes']['labels']['previous'] = [{'title': 'Acks::OK'},
                                                        {'title': 'somethingelse::OK'}]
        msg.payload['changes']['labels']['current'] = [{'title': 'Acks::OK'}]

        owners_parser = mock.Mock()
        self.assertTrue(ack_nack.process_mr_webhook(gl_instance, msg, owners_parser, 'mocked'))
        mock_process_mr.assert_called_with(gl_instance, gl_project, gl_mergerequest, owners_parser,
                                           'mocked', False)

    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_mr_webhook_ack_nack_label(self, mock_process_mr):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_instance.projects.get.return_value = gl_project
        gl_mergerequest = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        msg = mock.Mock()
        msg.payload = self._create_merge_payload('update')

        # Add ack_nack label
        msg.payload['changes']['labels']['previous'] = [{'title': 'somethingelse::OK'}]
        msg.payload['changes']['labels']['current'] = [{'title': 'Acks::OK'},
                                                       {'title': 'somethingelse::OK'}]

        owners_parser = mock.Mock()
        self.assertTrue(ack_nack.process_mr_webhook(gl_instance, msg, owners_parser, 'mocked'))
        mock_process_mr.assert_called_with(gl_instance, gl_project, gl_mergerequest, owners_parser,
                                           'mocked', True)

    def _create_merge_payload(self, action):
        return {'object_kind': 'merge_request',
                'user': {'id': 1, 'name': 'User 1', 'email': 'user1@redhat.com'},
                'project': {'id': 1, 'web_url': 'https://web.url/g/p'},
                'object_attributes': {'iid': 2, 'action': action,
                                      'last_commit': {'timestamp': '2021-01-08T20:00:00.000Z'}},
                'changes': {'labels': {'previous': [], 'current': []}}}

    def _test_approve_button_with(self, public_email, expected_email):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_instance.projects.get.return_value = gl_project
        gl_mergerequest = mock.Mock()
        gl_mergerequest.notes = mock.Mock()
        gl_mergerequest.notes.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_instance.users.get.return_value = mock.Mock(public_email=public_email)

        msg = mock.Mock()
        msg.payload = self._create_merge_payload('approved')
        self.assertTrue(ack_nack.process_mr_webhook(gl_instance, msg, 'mocked', 'mocked'))
        gl_mergerequest.notes.create.assert_called_with(
            {'body': f'Acked-by: User 1 <{expected_email}>\n(via approve button)'})

        msg.payload = self._create_merge_payload('unapproved')
        self.assertTrue(ack_nack.process_mr_webhook(gl_instance, msg, 'mocked', 'mocked'))
        gl_mergerequest.notes.create.assert_called_with(
            {'body': f'Rescind-acked-by: User 1 <{expected_email}>\n(via unapprove button)'})

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.ack_nack.process_merge_request', mock.Mock())
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_approve_button(self):
        # The test payload has user1@redhat.com hardcoded as the email field. Try various
        # public email addresses.
        self._test_approve_button_with('public-email@redhat.com', 'public-email@redhat.com')
        self._test_approve_button_with('user1@redhat.com', 'user1@redhat.com')
        self._test_approve_button_with(None, 'user1@redhat.com')
        self._test_approve_button_with('', 'user1@redhat.com')

    def test_filter_stale_reviewers(self):
        mock_mr = mock.Mock()
        stale_reviewers = ['someone', 'nobody']
        note1 = mock.Mock(id="1234", author={'username': 'cki-kwf-bot'},
                          body='requested review from @someone')
        note2 = mock.Mock(id="1235", author={'username': 'user35'},
                          body='requested review from @nobody')
        notes = mock.MagicMock()
        notes.list.return_value = [note1, note2]
        mock_mr.notes = notes
        output = ack_nack._filter_stale_reviewers(mock_mr, stale_reviewers)
        self.assertEqual(output, ['someone'])
        stale_reviewers = ['nobody', 'anotherone']
        output = ack_nack._filter_stale_reviewers(mock_mr, stale_reviewers)
        self.assertEqual(output, [])

    @mock.patch('webhook.ack_nack._emails_to_gl_user_names')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.common.mr_is_closed', mock.Mock(return_value=False))
    def test_assign_reviewers(self, mocked_users):
        mock_instance = mock.Mock()
        mock_mr = mock.MagicMock()
        mock_mr.iid = 222
        mock_mr.author = {'username': 'joeschmoe'}
        reviewers = ['joeschmoe@redhat.com', 'someone@redhat.com', 'nobody@redhat.com']
        sub_email = 'joeschmoe@redhat.com'
        assign = {'body': '/assign_reviewer @someone\n/assign_reviewer @nobody'}
        mocked_users.return_value = ['joeschmoe', 'someone', 'nobody']
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._assign_reviewers(mock_instance, mock_mr, reviewers, sub_email)
            self.assertIn("Assigning users ['someone', 'nobody'] to MR 222", logs.output[-1])
            mock_mr.notes.create.assert_called_with(assign)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.common.mr_is_closed', mock.Mock(return_value=False))
    def test_unassign_reviewers(self):
        mock_mr = mock.Mock()
        mock_mr.iid = 2
        users = ['someone', 'nobody']
        unassign = {'body': '/unassign_reviewer someone nobody'}
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._unassign_reviewers(mock_mr, users)
            self.assertIn("Unassigning users ['someone', 'nobody'] to MR 2", logs.output[-1])
            mock_mr.notes.create.assert_called_with(unassign)

    def test_get_existing_reviewers(self):
        mock_mr = mock.Mock()
        rev1 = {'id': 1, 'name': 'Some One', 'username': 'someone'}
        rev2 = {'id': 2, 'name': 'No Body', 'username': 'nobody'}
        rev3 = {'id': 3, 'name': 'Red Hatter', 'username': 'redhatter'}
        mock_mr.reviewers = [rev1, rev2, rev3]
        output = ack_nack._get_existing_reviewers(mock_mr)
        self.assertEqual(output, ['someone', 'nobody', 'redhatter'])

    def test_emails_to_gl_user_names(self):
        mock_instance = mock.Mock()
        user1 = mock.Mock(id="1", name="Some One", username="someone")
        user2 = mock.Mock(id="2", name="No Body", username="nobody")
        user3 = mock.Mock(id="3", name="Red Hatter", username="redhatter")
        reviewers = ['someone@redhat.com', 'nobody@redhat.com', 'redhatter@redhat.com']
        mock_instance.users.list.return_value = [user1, user2, user3]
        output = ack_nack._emails_to_gl_user_names(mock_instance, reviewers)
        self.assertEqual(output, {'someone', 'nobody', 'redhatter'})

    @mock.patch('webhook.ack_nack._emails_to_gl_user_names')
    def test_get_stale_reviewers(self, user_names):
        mock_instance = mock.Mock()
        old = ['user3']
        all_reviewers = [(['user1@redhat.com', 'user2@redhat.com'], 'subsys1')]
        user_names.return_value = ['user1', 'user2']
        output = ack_nack._get_stale_reviewers(mock_instance, old, all_reviewers)
        self.assertEqual(output, ['user3'])

    def test_get_old_subsystems_from_labels(self):
        mock_mr = mock.Mock()
        mock_mr.labels = ['Acks::net::NeedsReview', 'Acks::mm::OK', 'Something::whatever']
        (subsys, labels) = ack_nack._get_old_subsystems_from_labels(mock_mr)
        self.assertEqual(subsys, ['net', 'mm'])
        self.assertEqual(labels, ['Acks::net::NeedsReview', 'Acks::mm::OK'])

    def test_get_stale_labels(self):
        old_subsystems = ['net', 'mm', 'foobar']
        old_labels = ['Acks::net::NeedsReview', 'Acks::mm::OK', 'Acks::foobar::NACKed']
        subsys_scoped_labels = {'net': 'NeedsReview', 'mm': 'OK'}
        output = ack_nack._get_stale_labels(old_subsystems, old_labels, subsys_scoped_labels)
        self.assertEqual(output, ['Acks::foobar::NACKed'])

    @mock.patch('webhook.ack_nack._emails_to_gl_user_names')
    def test_report_stale_approvals(self, user_names):
        mock_inst = mock.Mock()
        mock_mr = mock.Mock()
        invalid = ['developer1@redhat.com']
        user_names.return_value = ['developer1']
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._report_stale_approvals(mock_inst, mock_mr, invalid)
            self.assertIn("Reported stale Approvals from: @developer1", logs.output[-1])
